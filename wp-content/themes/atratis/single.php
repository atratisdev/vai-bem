<?php 
	get_header();
	$imgtopdesk = get_field('img_top_desktop');  
	$imgtopmob = get_field('img_top_mobile');
?>
	<?php if($imgtopdesk){ ?>
    <section class="banner_blog wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="item-blog"
             style="background-image: url(<?php echo $imgtopdesk;?>); height: 279px; background-position: center;"
             alt="img" title="img">
        </div>
    </section>
    <?php } ?>
    <?php if($imgtopmob){ ?>
    <section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="item-blog">
            <img src="<?php echo $imgtopmob;?>" alt="img" title="img">
        </div>
    </section>
    <?php } ?>
    <div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="row">
            <div class="interna">
                <div class="breadcrumbs col-xs-12">
                    <?php dynamic_sidebar('breadcrumb'); ?>
                </div>
                <div class="col-xs-12">
                    <div class="post_interna">
                        <h1 class="titulo"><?php the_title(); ?></h1>
                        <div class="item">
                            <div class="box_texto">
                            	<h2><?php the_field("introducao");?></h2>
								<?php if ( has_post_thumbnail() ) { ?>
									<?php the_post_thumbnail('full');?>
								<?php } ?>
                                <?php echo do_shortcode(get_queried_object()->post_content);?>
                            </div>
                        </div>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe: </p>
                        <div class="box_midia">
                        	<div class="addthis_native_toolbox"></div>
							<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57a392c9f899ae52"></script>
                        </div>
                    </div>
                    <br clear="all"/>
                </div>
                <!--aside class="col-md-3">
					<div class="categoria">
						<h2>CATEGORIAS</h2>
						<ul>	
						<?php 
							wp_list_categories( array(
								'use_desc_for_title' => 0,		
								'hierarchical' => 1,
								'orderby' => 'name',
								'hide_empty' => 0,
								'title_li' =>"",
								'current_category' => 0,
								'child_of' => 4
							)); 
						?>
						</ul>
					</div>	        
			        <?php get_sidebar();?>
			      </aside-->
	         </div>
        </div>
    </div>

<?php get_footer(); ?>