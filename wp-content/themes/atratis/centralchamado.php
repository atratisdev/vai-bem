<?php
$chat = get_post(214);
$conteudo = $chat->post_content;
$args = array('post_type'=>'destino');
$q = new WP_Query( $args );
if ( $q->have_posts() ) {
?>

<div id="centroatende" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="chat-tit">CENTRAL DE ATENDIMENTO</h4>
			</div>
			<div class="modal-body">	
				<div class="row">
					<div class="col-xs-12 col-md-8 col-md-offset-2">
						<?php echo do_shortcode($conteudo);?>
					</div>
				</div>
				<?php wp_reset_postdata(); } ?>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>				
<script type="text/javascript">
	
</script>