<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Atratis - http://www.atratis.com"/>
    <meta name="google-site-verification" content=""/>
    <link rel="icon" href="<?php bloginfo('template_directory') ?>/favicon.png"/>
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.png" type="image/x-icon">
    <title>	    
		<?php
			wp_title( '|', true, 'right' );
			bloginfo( 'name' );
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) ){ echo " | $site_description"; }   
		?>
	</title>
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('stylesheet_directory'); ?>/images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('stylesheet_directory'); ?>/images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo('stylesheet_directory'); ?>/images/manifest.json">  
    
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>	
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>"/>	
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php bloginfo('stylesheet_directory'); ?>/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">  
	
    <?php wp_head(); ?>
	<script type="text/javascript">
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-83726699-1', 'auto');
	  ga('send', 'pageview');

      jQuery(window).load(function() {
          var wow = new WOW({
              boxClass: 'wow',
              animateClass: 'animated',
              offset: 100,
              mobile: false,
              live: true
          });
          wow.init();
      });
	</script>    
	
	<?php echo get_option("codanalytics");?>
</head>

<body <?php body_class(); ?> >

<!--[if lt IE 8]>
<p class="browserupgrade">Você está usando navegador <strong>ultrpassado</strong>. Por favor, <a href="http://browsehappy.com/">atualize seu navegador</a> para melhorar sua experiência.</p>
<![endif]-->

<header class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <button class="toggle">
                    <span></span><span></span><span></span>
                </button>
                <div class="topo">
                    <div class="menu-topo">
                        <h1 class="marca">
                            <a href="<?php echo home_url();?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt="<?php echo get_bloginfo('name');?>" /><?php //echo get_bloginfo('name');?></a>
                        </h1>
                        <nav class="menu">
							<?php
							   wp_nav_menu( array(
								   'menu'              => 'principal',
								   'theme_location'    => 'principal',
								   'depth'             => 3,
								   'container'         => '',
								   'container_class'   => '',
								   'container_id'      => '',
								   'menu_class'        => ''
							   ));
							?>
                        </nav>
                        <nav class="fr">
							<ul>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle acesse" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acesse sua área</a>
									<?php
									   wp_nav_menu( array(
										   'menu'              => 'login',
										   'theme_location'    => 'login',
										   'depth'             => 2,
										   'container'         => '',
										   'container_class'   => '',
										   'container_id'      => '',
										   'menu_class'        => 'dropdown-menu'
									   ));
									?>
								</li>
								<li><a id="adquira" target="_blank" href="<?php echo get_option('linkchat');?>" class="adquira">Atendimento Online</a></li>
							</ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
