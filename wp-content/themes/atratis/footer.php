		<footer class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.3s">
		    <div class="container">
		        <div class="row">
		            <div class="col-md-3 col-xs-12">
		                <div class="marca">
		                    <a href="<?php echo home_url();?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt="Vai bem" title="Vai bem"></a>
		                </div>
		            </div>
		            <div class="col-md-7 col-xs-12">
		                <nav>
							<?php if ( has_nav_menu( 'institucional' ) ) { ?>
							<div class="box-menu">
								<h3>Institucional</h3>
								<?php
								   wp_nav_menu( array(
									   'menu'              => 'institucional',
									   'theme_location'    => 'institucional',
									   'depth'             => 1,
									   'container'         => '',
									   'container_class'   => '',
									   'container_id'      => '',
									   'menu_class'        => ''
								   ));
								?>
							</div>
							<?php } ?>
							
							<?php if ( has_nav_menu( 'legalprivacidade' ) ) { ?>
							<div class="box-menu">
								<h3>Legal e Privacidade</h3>
								<?php
								   wp_nav_menu( array(
									   'menu'              => 'legalprivacidade',
									   'theme_location'    => 'legalprivacidade',
									   'depth'             => 1,
									   'container'         => '',
									   'container_class'   => '',
									   'container_id'      => '',
									   'menu_class'        => ''
								   ));
								?>
							</div>
							<?php } ?>
							
							<?php if ( has_nav_menu( 'recursos' ) ) { ?>
							<div class="box-menu">
								<h3>Recursos</h3>
								<?php
								   wp_nav_menu( array(
									   'menu'              => 'recursos',
									   'theme_location'    => 'recursos',
									   'depth'             => 1,
									   'container'         => '',
									   'container_class'   => '',
									   'container_id'      => '',
									   'menu_class'        => ''
								   ));
								?>
							</div>
							<?php } ?>
		                </nav>
		            </div>
		            <?php dynamic_sidebar('rodape'); ?>
		            <div class="col-xs-12">
		            	<?php dynamic_sidebar('copyright'); ?>
		            </div>
		        </div>
		    </div>
		</footer>


		<?php 
			wp_footer();
			get_template_part("chat"); 
			get_template_part("centralchamado");
			get_template_part("alertapadrao");
		?>
	</body>
</html>