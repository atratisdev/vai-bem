<?php get_header(); ?>

<section class="banner_principal wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('destaque'); ?>
</section>

<section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('destaquemob'); ?>	
</section>

<p class="barra wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
    <?php dynamic_sidebar('home1'); ?>
</p>

<section class="porque wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home2'); ?>
	<span class="bg_fun_02"></span>	
</section>

<section class="funciona wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home3'); ?>
	<span class="bg_fun_02"></span>
</section>

<section class="videos  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home4'); ?>
</section>

<section class="newsletter  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home5'); ?>
</section>

<?php get_footer(); ?>