<?php
/**
 * Template Name: Pismo
 */
$acao = $_GET['acao'];
$urltoken = get_option('requsertoken');
$emailpismo = get_option('emailpismo');
$senhapismo = get_option('senhapismo');
$linkpismo = get_option('linkpismo');	
$funccadcli = get_option('funccadcliente');
$funclocsea = get_option('funcsearchplace');
if($acao=="cliente"){
	$url = $linkpismo.$funccadcli;
}elseif($acao=="locais"){	
	$url = $linkpismo.$funclocsea;
}else{
	echo "requisição inválida";
	exit;
}
?><!DOCTYPE html>
<html>
<head>	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
    <title>Pismo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/fakeLoader.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/fakeLoader.min.js"></script>
	
</head>
<body>
<div id="fakeloader"></div>
<form id="formpismo" style="display:none;"><input id="envio" name="envio" type="submit" /></form>

<script type="text/javascript">
$( document ).ready(function() {
	$("#fakeloader").fakeLoader({
		timeToHide:1200,
		zIndex:"999",
		spinner:"spinner1",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
		bgColor:"#702d8c"
	});
});
</script>
<script type="text/javascript">

var form = document.getElementById("formpismo");

document.onreadystatechange=function(){
	document.getElementById('envio').click();
	//document.forms[0].submit();
}

form.addEventListener('submit', function(evt) {
  evt.preventDefault();

  var data = {
    //email: document.querySelector('input[name="email"]').value,
    email: <?php echo '"'.$emailpismo.'"';?>,
    //password: document.querySelector('input[name="password"]').value
	password: <?php echo '"'.$senhapismo.'"';?>  
  }

  var xhr = new XMLHttpRequest();
  xhr.open("POST", <?php echo '"'.$urltoken.'"';?>);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(data));

  xhr.onreadystatechange = function(state) {
      if(xhr.readyState === XMLHttpRequest.DONE) {
        //document.querySelector('#result').textContent = 'Token : ' + JSON.parse(xhr.responseText).token;
		var token = JSON.parse(xhr.responseText).token;
		var tenant = JSON.parse(xhr.responseText).tenant;
		var destino = "<?php echo $url;?>?token="+token+"&tenant="+tenant;
		window.location.href = destino;
      }
  };
});

</script>
</body>
</html>