<?php
/*
 * Funções de configuração e instalação
 */
//error_reporting(E_ALL); 
//ini_set('display_errors', 1);
remove_action('wp_head', 'wp_generator');
  
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks'); 
 
function instalacao_tema() {
	add_editor_style();
	add_theme_support('post-thumbnails');	
	add_image_size( 'capa-livro', 175, 276 );	
	add_image_size( 'imagem-artigo', 600, 400 ); 
	register_nav_menus(array(
		'principal'     	=> __('Menu Principal'),
		'login'     		=> __('Menu Login'),
		'institucional'     => __('Menu Institucional'),
		'legalprivacidade'  => __('Menu Legal e Privacidade'),
		'recursos'    		=> __('Menu Recursos'),
		'categoriasmenu'	=> __('Menu Categorias')
	));

	if (class_exists('MultiPostThumbnails')) {
		$types = array('post', 'page', 'my_post_type');
		foreach($types as $type) {
			new MultiPostThumbnails(array(
				'label' => 'Imagem destacada 2',
				'id' => 'feature-image-2',
				'post_type' => $type
				)
			);
		}
	}	
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

}
add_action( 'after_setup_theme', 'instalacao_tema' );
	
function area_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Destaque'),
		'id'            => 'destaque',
		'description'   => __( 'Widget na posição destaque' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));	
	register_sidebar( array(
		'name'          => __( 'Destaque Mobile'),
		'id'            => 'destaquemob',
		'description'   => __( 'Widget na posição destaque mobile' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));	
	register_sidebar( array(
		'name'          => __( 'Navegação Estrutural'),
		'id'            => 'breadcrumb',
		'description'   => __( 'Widget no espaço para o Breadcrumb' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));	
	register_sidebar( array(
		'name'          => __( 'Home 1'),
		'id'            => 'home1',
		'description'   => __( 'Widgets na posição Home 1' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));
	register_sidebar( array(
		'name'          => __( 'Home 2'),
		'id'            => 'home2',
		'description'   => __( 'Widgets na posição Home 2' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));
	register_sidebar( array(
		'name'          => __( 'Home 3'),
		'id'            => 'home3',
		'description'   => __( 'Widgets na posição Home 3' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));	
	register_sidebar( array(
		'name'          => __( 'Home 4'),
		'id'            => 'home4',
		'description'   => __( 'Widgets na posição Home 4' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));	
	
	register_sidebar( array(
		'name'          => __( 'Home 6'),
		'id'            => 'home6',
		'description'   => __( 'Widgets na posição Home 6' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));	
	
	register_sidebar( array(
		'name'          => __( 'Home 5'),
		'id'            => 'home5',
		'description'   => __( 'Widgets na posição Home 5' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));		
	register_sidebar( array(
		'name'          => __( 'Lateral Blog'),
		'id'            => 'lateral',
		'description'   => __( 'Widget no espaço lateral do Blog' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));			
	register_sidebar( array(
		'name'          => __( 'Rodapé'),
		'id'            => 'rodape',
		'description'   => __( 'Widgets na posição rodapé' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));	
	register_sidebar( array(
		'name'          => __( 'Copyright'),
		'id'            => 'copyright',
		'description'   => __( 'Widgets na posição copyright' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));
}
add_action( 'widgets_init', 'area_widgets_init' );


function add_scripts() {
	if ( !is_admin() ) {
		wp_enqueue_script('jquery',get_template_directory_uri() . '/js/jquery.min.js','','',true);
		wp_enqueue_script('scripts',get_template_directory_uri() . '/js/scripts.min.js','','',true);
		wp_enqueue_script('cycle',get_template_directory_uri() . '/js/jquery.cycle2.js','','',true);
		wp_enqueue_script('cycle-carousel',get_template_directory_uri() . '/js/jquery.cycle2.carousel.js','','',true);
		wp_enqueue_script('mask', get_template_directory_uri() . '/js/jquery.maskedinput.min.js','','',true );
		wp_enqueue_script('site', get_template_directory_uri() . '/js/site.js','','',true );
		wp_enqueue_script('site2', get_template_directory_uri() . '/js/site2.js','','',true );
		wp_enqueue_style( 'estilo', get_template_directory_uri() . '/css/tema.php' );
	}
}
add_action( 'wp_enqueue_scripts', 'add_scripts' );

function get_depth($id = '', $depth = '', $i = 0){
    global $wpdb;
    if($depth == ''){
        if(is_page()){
                if($id == ''){
                        global $post;
                        $id = $post->ID;
                }
                $depth = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID = '".$id."'");
                return get_depth($id, $depth, $i);
        }elseif(is_category()){
            if($id == ''){
                    global $cat;
                    $id = $cat;
            }
            $depth = $wpdb->get_var("SELECT parent FROM $wpdb->term_taxonomy WHERE term_id = '".$id."'");
            return get_depth($id, $depth, $i);
        }
        elseif(is_single()){
            if($id == ''){
                    $category = get_the_category();
                    $id = $category[0]->cat_ID;
            }
            $depth = $wpdb->get_var("SELECT parent FROM $wpdb->term_taxonomy WHERE term_id = '".$id."'");
            return get_depth($id, $depth, $i);
        }
    }
    elseif($depth == '0'){
            return $i;
    }
    elseif(is_single() || is_category()){
            $depth = $wpdb->get_var("SELECT parent FROM $wpdb->term_taxonomy WHERE term_id = '".$depth."'");
            $i++;
            return get_depth($id, $depth, $i);
    }
    elseif(is_page()){
            $depth = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID = '".$depth."'");
            $i++;
            return get_depth($id, $depth, $i);
    }
}  

add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
		return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
); 

add_filter('single_template', create_function(
'$the_template',
'foreach( (array) get_the_category() as $cat ) {
            if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") ){
                return TEMPLATEPATH . "/single-{$cat->slug}.php";
            }else if($cat->category_parent){
                $parent = get_category($cat->category_parent);
                if ( file_exists(TEMPLATEPATH . "/single-{$parent->slug}.php") )
                    return TEMPLATEPATH . "/single-{$parent->slug}.php";
                }
            }
            return $the_template;' )
);

function use_single_parent_template() {

	if ( !is_category() || is_404() ) return; 

	$cat = get_query_var('cat');
	$category = get_category ($cat);

	if ( !($category) ) return;

	if ( file_exists(TEMPLATEPATH . '/single-' . $category->cat_ID . '.php') ) {
		include(TEMPLATEPATH . '/single-' . $category ->cat_ID . '.php');
		exit; }
		elseif ( file_exists(TEMPLATEPATH . '/single-' . $category->category_parent . '.php') ) {
			include(TEMPLATEPATH . '/single-' . $category->category_parent . '.php');
			exit; }
}
add_action('template_redirect', 'use_single_parent_template');

function inherit_template() {
	if (is_category()) {
		$catid = get_query_var('cat');
		if ( file_exists(TEMPLATEPATH . '/category-' . $catid . '.php') ) {
			include( TEMPLATEPATH . '/category-' . $catid . '.php');
			exit;
		}

		$cat = &get_category($catid);
		$parent = $cat->category_parent;
		while ($parent) {
			$cat = &get_category($parent);
			if ( file_exists(TEMPLATEPATH . '/category-' . $cat->cat_ID . '.php') ) {
				include (TEMPLATEPATH . '/category-' . $cat->cat_ID . '.php');
				exit;
			}

			$parent = $cat->category_parent;
		}
	}
}

add_action('template_redirect', 'inherit_template', 1);
	
class wp_bootstrap_navwalker extends Walker_Nav_Menu {

        public function start_lvl( &$output, $depth = 0, $args = array() ) {
                $indent = str_repeat( "\t", $depth );
                $output .= "\n$indent<ul role=\"menu\" class=\" multi-level dropdown-menu\">\n";
        }

        public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
                $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

                if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
                        $output .= $indent . '<li role="presentation" class="divider">';
                } else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
                        $output .= $indent . '<li role="presentation" class="divider">';
                } else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
                        $output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
                } else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
                        $output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
                } else {

                        $class_names = $value = '';

                        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
                        $classes[] = 'menu-item-' . $item->ID;

                        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

                        if ( $args->has_children )
                                $class_names .= ' dropdown ';

                        if ( in_array( 'current-menu-item', $classes ) )
                                $class_names .= ' active';

                        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

                        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
                        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

                        $output .= $indent . '<li' . $id . $value . $class_names .'>';

                        $atts = array();
                        $atts['title']  = ! empty( $item->title )	? $item->title	: '';
                        $atts['target'] = ! empty( $item->target )	? $item->target	: '';
                        $atts['rel']    = ! empty( $item->xfn )		? $item->xfn	: '';

                        // If item has_children add atts to a.
                        if ( $args->has_children && $depth === 0 ) {
                                $atts['href']   		= '#';
                                $atts['data-toggle']	= 'dropdown';
                                $atts['class']			= 'dropdown-toggle';
                                $atts['aria-haspopup']	= 'true';
                                $atts['aria-expanded']	= 'false';
                        } else {
                                $atts['href'] = ! empty( $item->url ) ? $item->url : '';
                        }

                        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

                        $attributes = '';
                        foreach ( $atts as $attr => $value ) {
                                if ( ! empty( $value ) ) {
                                        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                                        $attributes .= ' ' . $attr . '="' . $value . '"';
                                }
                        }

                        $item_output = $args->before;

                        if ( ! empty( $item->attr_title ) )
                                $item_output .= '<a'. $attributes .'><span class="glyphicon ' . esc_attr( $item->attr_title ) . '"></span>&nbsp;';
                        else
                                $item_output .= '<a'. $attributes .'>';

                        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
                        $item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="caret"></span></a>' : '</a>';
                        $item_output .= $args->after;

                        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
                }
        }

        public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;

        $id_field = $this->db_fields['id'];

        if ( is_object( $args[0] ) )
           $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }


    public static function fallback( $args ) {
        if ( current_user_can( 'manage_options' ) ) {
            extract( $args );
            $fb_output = null;
            if ( $container ) {
                    $fb_output = '<' . $container;
                    if ( $container_id )
                            $fb_output .= ' id="' . $container_id . '"';
                    if ( $container_class )
                            $fb_output .= ' class="' . $container_class . '"';
                    $fb_output .= '>';
            }
            $fb_output .= '<ul';
            if ( $menu_id )
               $fb_output .= ' id="' . $menu_id . '"';
            if ( $menu_class )
                    $fb_output .= ' class="' . $menu_class . '"';
            $fb_output .= '>';
            $fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Adicione um Menu</a></li>';
            $fb_output .= '</ul>';
            if ( $container )
                    $fb_output .= '</' . $container . '>';
            echo $fb_output;
    	}
    }
}

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
	$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
	return $html;
}

function SearchFilter($query) {
	if ($query->is_search && !is_admin()) {
		$query->set('post_type', 'post');
		$query->set('cat','4');
	}
	return $query;
}
if(!is_admin()){
	add_filter('pre_get_posts','SearchFilter');
}
add_filter( 'wp_list_categories', 'sgr_show_current_cat_on_single', 10, 2 );

function sgr_show_current_cat_on_single( $output, $args ) {

  if ( is_single() ) :

    global $post;

    $terms = get_the_terms( $post->ID, $args['taxonomy'] );

    foreach( $terms as $term ) {

      if ( preg_match( '#cat-item-' . $term ->term_id . '#', $output ) ) {
        $output = str_replace('cat-item-'.$term ->term_id, 'cat-item-'.$term ->term_id . ' current-cat', $output);
      }

    }

  endif;

  return $output;

}

function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

?>