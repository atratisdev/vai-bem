<?php
/**
 * Template Name: Formulários
 */
?>
<?php 
	get_header();
	$imgtopdesk = get_field('img_top_desktop');  
	$imgtopmob = get_field('img_top_mobile');
?>
	<?php if($imgtopdesk){ ?>
    <section class="banner_blog wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="item-blog"
             style="background-image: url(<?php echo $imgtopdesk;?>); height: 279px; background-position: center;"
             alt="img" title="img">
        </div>
    </section>
    <?php } ?>
    <?php if($imgtopmob){ ?>
    <section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="item-blog">
            <img src="<?php echo $imgtopmob;?>" alt="img" title="img">
        </div>
    </section>
    <?php } ?>
    <div class="container fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="row">
            <div class="interna">
                <div class="breadcrumbs col-xs-12">
                    <?php dynamic_sidebar('breadcrumb'); ?>
                </div>
                <div class="col-xs-12">
                    <div class="post_interna">
                        <h1 class="titulo"><?php the_title(); ?></h1>
                        <div class="item">
                            <div class="box_texto">
                                <h2><?php the_field("introducao");?></h2>
                                <?php the_post_thumbnail("full");?>
                                <?php echo do_shortcode(get_queried_object()->post_content);?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>