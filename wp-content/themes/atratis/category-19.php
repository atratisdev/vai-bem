<?php 
get_header(); 
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$imgtopdesk = get_field('img_top_desktop',$taxonomy.'_'.$term_id);
$imgtopmob = get_field('img_top_mobile',$taxonomy.'_'.$term_id);

?>
<section class="banner_blog wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">        
	<div class="item-blog" style="background-image: url(<?php echo $imgtopdesk;?>); height: 279px; background-position: center;" alt="img" title="img"></div>
</section>
<section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="item-blog">
		<img src="<?php echo $imgtopmob;?>" alt="img" title="img">
	</div>
</section>
<div class="container fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="row">
	<div class="interna">
        <div class="breadcrumbs col-xs-12">
        	<?php dynamic_sidebar('breadcrumb'); ?>
        </div>
		<div class="col-xs-12">
			<h1 class="titulo"><?php single_cat_title();?></h1>
		</div>

		<div class="col-md-9">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>        
			<div class="post">
				<div class="item">        
					<div class="box_texto">            
						<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
						<p><?php the_excerpt();?></p>
						<a href="<?php the_permalink();?>" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		<?php endwhile; endif;?>

        <br clear="all" />

        <nav class="pag">
			<?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>
        </nav>
      </div>
      <aside class="col-md-3">
      	<?php get_sidebar();?>
      </aside>


    </div>
  </div>
</div>

<?php get_footer(); ?>