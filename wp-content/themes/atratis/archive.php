<?php get_header();  ?>
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="row">
	<div class="interna">
        <div class="breadcrumbs col-xs-12">
        	<?php dynamic_sidebar('breadcrumb'); ?>
        </div>
		<div class="col-xs-12">
			<h1 class="titulo">
                <?php /* If this is a category archive */ if (is_category()) { ?>
                    Arquivos para &#8216;<?php single_cat_title(); ?>&#8217;
                <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
                    Posts da tag &#8216;<?php single_tag_title(); ?>&#8217;:
                <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
                    Arquivos para <?php the_time('F jS, Y'); ?>
                <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
                    Arquivos para <?php the_time('F, Y'); ?>
                <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
                    Arquivos para <?php the_time('Y'); ?>
                <?php /* If this is an author archive */ } elseif (is_author()) { ?>
                    Arquivos do Autor
                <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
                    Arquivos do Blog
                <?php } ?>			
			</h1>
			<?php echo category_description();?>
		</div>

		<div class="col-md-9">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>        
			<div class="post">
				<div class="item">          
					<div class="imagem">
						<?php if ( has_post_thumbnail() ) { ?>
						<a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a>
						<?php }else{ ?>
						<img src="<?php echo get_template_directory_uri()."/images/semfoto.png";?>" alt="<?php the_title();?>" />
						<?php } ?>
					</div>          
					<div class="box_texto">            
					<div class="data"><?php echo get_the_date( 'd/m/Y' ); ?></div>
						<div class="tags">
						<?php 
							$categories = get_the_category();							
							foreach($categories as $category){
						?> 
							<a title="<?php echo $category->name; ?>" href="<?php echo esc_url( get_category_link( $category->term_id ) );?>" ><?php echo $category->name; ?></a>
						<?php } ?>
						</div>
						<h2>
			                <a href="<?php the_permalink();?>"><?php the_title();?></a>
						</h2>
						<p><?php the_excerpt();?></p>
						<a href="<?php the_permalink();?>" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
	        <br clear="all" />
	
	        <nav class="pag">
				<?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>
	        </nav>			
			<?php endwhile; else: ?>
			<div class="post">
				<div class="item"> 
					<div class="box_erro">
						<h4>Nenhum item encontrado!</h4>
						<p>Esta categoria não possui itens ainda!</p>
					</div>
				</div>
			</div>				
			<?php endif;?>

      </div>
      <aside class="col-md-3">
      	<?php get_sidebar();?>
      </aside>


    </div>
  </div>
</div>

<?php get_footer(); ?>