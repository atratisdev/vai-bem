jQuery(document).ready(function($) {
	jQuery("#telefone").mask("(99)9999.99999", {autoclear: false});
	jQuery("#cep").mask("99.999-99");
	jQuery('#menu-principal li a').each(function(){
		jQuery(this).wrapInTag({
			tag: 'strong',
			words: ['vai','bem','rede ','credenciada']
		});		
  	});
	jQuery("#menu-item-212 a").html(remove_tags(jQuery("#menu-item-212 a").html()));	
	jQuery("#menu-item-233 a").html(remove_tags(jQuery("#menu-item-233 a").html()));


	
	jQuery(".cycle-pager span").click(function(){
		jQuery("iframe").each(function() {
			jQuery(this)[0].contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*')
		});
		//jQuery('iframe').contents().find('video').each(function () 
        //{
        //    this.currentTime = 0;
        //    this.pause();
        //});		
	});
	//jQuery(".sub-menu").hide();
	//jQuery("li.rede_conveniada a").hover(function(event){
	//	event.preventDefault();
	//	jQuery(".sub-menu").toggle();
	//});
	
	jQuery("#adquira").click(function( event ) {
		event.preventDefault();
		jQuery("#chatonline").modal();
	});

	jQuery("#adquiraja").click(function( event ) {
		event.preventDefault();
		jQuery("#chatonline").modal();
	});
	
	
	jQuery("#centraltel").click(function( event ) {
		event.preventDefault();
		jQuery("#centroatende").modal();
	});	
	
	jQuery("a").bind('tap click',function(event){
		var ahash = jQuery(this).attr('href').replace(/^.*?(#|$)/,'');
		console.log(ahash);
		if(ahash=='adquira'){
			event.preventDefault();
			jQuery("#centroatende").modal();
		}
	});
	
	jQuery("#enviochat").on('click',function(){
		var destino = jQuery("#destino").val();
		var gattr = jQuery("#destino option:selected").attr('dest');
 		console.log(gattr);
		if(destino==""){
			alert("Selecione um item.");
		}else{
			if(gattr=="popup"){
				window.open(destino,"Atendimento Online","width=520,height=480,toolbar=no,status=no,scrollbars=no");
			}else{
				window.location.href = destino;
				window.open(destino, "_blank");
			}
			
		}	
	});	
	
});

jQuery.fn.wrapInTag = function(opts) {
  
  var tag = opts.tag || 'strong',
      words = opts.words || [],
      regex = RegExp(words.join('|'), 'gi'),
      replacement = '<'+ tag +'>$&</'+ tag +'>';
  
  return this.html(function() {
    return jQuery(this).text().replace(regex, replacement);
  });
};

function remove_tags(html){
     return jQuery(html).text();
}

function chamamodal(str){
	jQuery("#alertahtml").html(str);
	jQuery("#alertapadrao").modal();
}

