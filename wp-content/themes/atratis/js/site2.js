jQuery(document).ready(function($) {
	
	jQuery(".adquira").click(function( event ) {
		event.preventDefault();
		jQuery("#chatonline").modal();
	});

	jQuery(".centraltel").click(function( event ) {
		event.preventDefault();
		jQuery("#centroatende").modal();
	});		
	
	if ( jQuery(".carousel-indicators li").length <= 1 ) {
		jQuery( ".carousel-indicators li" ).each( function( index, el ){
			jQuery( el ).addClass( "hide" );
			jQuery(".carousel-control").addClass("hide");
		});
	}
	
});