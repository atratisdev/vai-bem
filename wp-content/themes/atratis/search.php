<?php get_header(); ?>
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="row">
	<div class="interna">
        <div class="breadcrumbs col-xs-12">
        	<?php dynamic_sidebar('breadcrumb'); ?>
        </div>
		<div class="col-xs-12">
			<h2 class="titulo">Resultado da busca por: <?php echo get_search_query();?></h2>
		</div>

		<div class="col-md-9">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>        
			<div class="post">
				<div class="item">          
					<div class="imagem">
						<?php if ( has_post_thumbnail() ) { ?>
						<a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a>
						<?php }else{ ?>
						<img src="<?php echo get_template_directory_uri()."/images/semfoto.png";?>" alt="<?php the_title();?>" />
						<?php } ?>
					</div>          
					<div class="box_texto">            
						<h2>
			                <a href="<?php the_permalink();?>"><?php the_title();?></a>
						</h2>
						<p><?php the_excerpt();?></p>
						<a href="<?php the_permalink();?>" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
	        <br clear="all" />		
			<?php endwhile; ?>
			<nav class="pag">
				<?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>
	        </nav>	
			<?php else: ?>
			<div class="post">
				<div class="item"> 
					<div class="box_erro">
						<h4>Nenhum item encontrado!</h4>
						<p>Esta categoria não possui itens ainda!</p>
					</div>
				</div>
			</div>				
			<?php endif;?>

      </div>
      <aside class="col-md-3">
      	<?php get_sidebar();?>
      </aside>


    </div>
  </div>
</div>

<?php get_footer(); ?>