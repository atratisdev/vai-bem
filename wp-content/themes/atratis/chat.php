<?php
$chat = get_post(133);
$conteudo = $chat->post_content;
$args = array('post_type'=>'destino');
$q = new WP_Query( $args );
if ( $q->have_posts() ) {
?>

<div id="chatonline" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="chat-tit">CHAT ONLINE</h4>
			</div>
			<div class="modal-body">	
				<div class="row">
					<div class="col-xs-12">
						<p>Olá, selecione o assunto ou área com quem deseja falar:</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-8 col-md-offset-2">

						<?php //echo do_shortcode($conteudo);?>
						<select name="destino" id="destino" class="form-control">
							<option value="">Selecione...</option>
							<?php while ( $q->have_posts() ) { $q->the_post(); ?>
								<option dest="<?php echo get_field("popup");?>" value="<?php echo get_the_excerpt();?>"><?php echo get_the_title();?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-xs-12 col-md-8 col-md-offset-2">
						<button id="enviochat" class="btn btn-primary">Selecionar</button>
					</div>
				</div>
				<?php wp_reset_postdata(); } ?>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>				
<script type="text/javascript">
	
</script>