<?php get_header(); ?>
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="row">
		<div class="interna">
        	<div class="breadcrumbs col-xs-12">
				<?php dynamic_sidebar('breadcrumb'); ?>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="box-conteudo">
				<h1 class="tit">Página não encontrada</h1>
				<div class="box-texto">
					<h2>Página não encontrada. A página que você tentou acessar não existe ou está temporariamente indisponível. Que tal conhecer o site navegando no menu?</h2>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>