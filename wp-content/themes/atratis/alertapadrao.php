<div id="alertapadrao" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="chat-tit">MENSAGEM</h4>
			</div>
			<div class="modal-body">	
				<div class="row">
					<div class="col-xs-12 col-md-8 col-md-offset-2">
						<p id="alertahtml"></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>