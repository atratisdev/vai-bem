<div class="relacionados">
<?php
	GLOBAL $post; 
	$categories = get_the_category();
	$cats = array();
	foreach($categories as $category){ $cats[] = $category->term_id;}
	$cals = implode(",",$cats);
	if ($cals) {
		$first_tag = $tags[0]->term_id;
		$args=array(
			'category__in' => array($cals),
			'category__not_in' => array(1,2,19),
			'post__not_in' => array($post->ID),
			'posts_per_page'=>3,
			'caller_get_posts'=>1
		);
		$queryx = new WP_Query($args);
		if( $queryx->have_posts() ) {
			while ($queryx->have_posts()) : $queryx->the_post(); 
?>
	<div class="item">
		<div class="box_img">
			<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
				<?php 
					if ( has_post_thumbnail() ) {
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'thumbnail' );
						$url = $thumb['0'];
					}else{
						$url = get_template_directory_uri()."/images/semfoto.png";
					}
				?>
				<img src="<?php echo $url;?>" alt="<?php the_title(); ?>" />	
			</a>
		</div>
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
	</div>
<?php
		endwhile;
	}
	wp_reset_query();
}
?>
</div>