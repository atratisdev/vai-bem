<form action="<?php echo esc_url(home_url('/')); ?>" id="searchform" method="get">

    <input type="search" value="<?php the_search_query(); ?>" id="s" name="s" class="inp" placeholder="Procurar..." />
    <input type="submit" value="Buscar" id="searchsubmit" class="lupa" />

</form>