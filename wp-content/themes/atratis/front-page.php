<?php get_header(); ?>

<?php if ( is_active_sidebar( 'destaque' ) ) { ?>
<section class="banner_principal wow fadeInDown  hidden-xs hidden-sm"  data-wow-duration="0s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('destaque'); ?>
</section>
<?php } ?>

<?php if ( is_active_sidebar( 'destaquemob' ) ) { ?>
<section class="banner-mob wow fadeInDown hidden-lg hidden-md"  data-wow-duration="0s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('destaquemob'); ?>
</section>
<?php } ?>

<?php if ( is_active_sidebar( 'home1' ) ) { ?>
<p class="barra wow fadeInDown"  data-wow-duration="0s" data-wow-delay="0.5s">
    <?php dynamic_sidebar('home1'); ?>
</p>
<?php } ?>

<?php if ( is_active_sidebar( 'home2' ) ) { ?>
<section class="porque wow fadeInDown"  data-wow-duration="0s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home2'); ?>
	<span class="bg_fun_02"></span>
</section>
<?php } ?>

<?php if ( is_active_sidebar( 'home3' ) ) { ?>
<section class="funciona wow fadeInDown"  data-wow-duration="0s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home3'); ?>
	<span class="bg_fun_02"></span>
</section>
<?php } ?>

<?php if ( is_active_sidebar( 'home4' ) ) { ?>
<section class="economia  wow fadeInDown"  data-wow-duration="0s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home4'); ?>
</section>
<?php } ?>

<?php if ( is_active_sidebar( 'home5' ) ) { ?>
<section class="videos  wow fadeInDown"  data-wow-duration="0s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home5'); ?>
</section>
<?php } ?>

<?php if ( is_active_sidebar( 'home6' ) ) { ?>
<section class="newsletter  wow fadeInDown"  data-wow-duration="0s" data-wow-delay="0.5s">
	<?php dynamic_sidebar('home6'); ?>
</section>
<?php } ?>

<?php get_footer(); ?>