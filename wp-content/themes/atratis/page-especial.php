<?php
/**
 * Template Name: Vai Bem Para Todos
 */
?>
<?php 
	get_header();
	$imgtopdesk = get_field('img_top_desktop');  
	$imgtopmob = get_field('img_top_mobile');
	$linkimgtop = get_field('link_img_topo');
	$novajanela = get_field('abrir_em_nova_janela');
?>
<section class="banner_principal wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
        
    <div id="banner-topo" class="carousel slide" data-ride="carousel">
      <!-- Indicators 
      <ol class="carousel-indicators">
        <li data-target="#banner-topo" data-slide-to="0" class="active"></li>
        <li data-target="#banner-topo" data-slide-to="1"></li>
        <li data-target="#banner-topo" data-slide-to="2"></li>
      </ol>-->

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <a target="<?php echo $novajanela;?>" href="<?php echo $linkimgtop;?>" class="item active" style="background-image: url(<?php echo $imgtopdesk;?>);" alt="img" title="img">
        </a>      
      </div>

      <!-- Controls 
      <a class="left carousel-control" href="#banner-topo" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#banner-topo" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      -->
    </div>
</section>

<section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
        
    <div id="banner-mob" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <a href="<?php echo $linkimgtop;?>" class="item active">
            <img src="<?php echo $imgtopmob;?>" alt="img" title="img">
        </a>       
      </div>

      <!-- Controls 
      <a class="left carousel-control" href="#banner-mob" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#banner-mob" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      -->
    </div>
</section>

<div class="container fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="row">
		<div class="col-xs-12">
      		<div class="interna">
				<h1 class="titulo"><?php the_title();?></h1>
				<?php the_post_thumbnail("full");?>
				<?php echo do_shortcode(get_queried_object()->post_content);?>
				<div class="compartilhe">
					<p>Compartilhe: </p>
					<div class="box_midia">
                    	<div class="addthis_native_toolbox"></div>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57a392c9f899ae52"></script>
                   	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>