<?php 
get_header(); 
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$imgtopdesk = get_field('img_top_desktop',$taxonomy.'_'.$term_id);
$imgtopmob = get_field('img_top_mobile',$taxonomy.'_'.$term_id);

?>
<section class="banner_blog wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">        
	<div class="item-blog" style="background-image: url(<?php echo $imgtopdesk;?>); height: 279px; background-position: center;" alt="img" title="img"></div>
</section>
<section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="item-blog">
		<img src="<?php echo $imgtopmob;?>" alt="img" title="img">
	</div>
</section>
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="row">
	<div class="interna">
        <div class="breadcrumbs col-xs-12">
        	<?php dynamic_sidebar('breadcrumb'); ?>
        </div>
		<div class="col-xs-12">
			<h1 class="titulo"><?php single_cat_title();?></h1>
			<?php echo category_description();?>
		</div>

		<div class="col-md-9">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>        
			<div class="post">
				<div class="item">          
					<div class="imagem">
						<?php if ( has_post_thumbnail() ) { ?>
						<a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a>
						<?php }else{ ?>
						<img src="<?php echo get_template_directory_uri()."/images/semfoto.png";?>" alt="<?php the_title();?>" />
						<?php } ?>
					</div>          
					<div class="box_texto">            
					<div class="data"><?php echo get_the_date( 'd/m/Y' ); ?></div>
						<div class="tags">
						<?php 
							$categories = get_the_category();							
							foreach($categories as $category){
						?> 
							<a title="<?php echo $category->name; ?>" href="<?php echo esc_url( get_category_link( $category->term_id ) );?>" ><?php echo $category->name; ?></a>
						<?php } ?>
						</div>
						<h2>
			                <a href="<?php the_permalink();?>"><?php the_title();?></a>
						</h2>
						<p><?php the_excerpt();?></p>
						<a href="<?php the_permalink();?>" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
	        <br clear="all" />
	
	        <nav class="pag">
				<?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>
	        </nav>			
			<?php endwhile; else: ?>
			<div class="post">
				<div class="item"> 
					<div class="box_erro">
						<h4>Nenhum item encontrado!</h4>
						<p>Esta categoria não possui itens ainda!</p>
					</div>
				</div>
			</div>				
			<?php endif;?>

      </div>
      <aside class="col-md-3">
      	<?php get_sidebar();?>
      </aside>


    </div>
  </div>
</div>

<?php get_footer(); ?>