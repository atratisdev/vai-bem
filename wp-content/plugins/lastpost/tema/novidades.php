
		<div class="row">
			<div class="col-xs-12">
				<h2 class="titulo-laranja"><?php echo $titulo;?> <span>TRAXX</span></h2>
			</div>
			<?php while (have_posts()) : the_post(); $imagem = get_the_post_thumbnail(get_the_ID()); ?>
			<div class="col-xs-12 col-md-4">    
				<a title="<?php the_title();?>" href="<?php the_permalink();?>">
					<?php echo $imagem;?>
					<h3><?php the_title();?></h3>
					<?php the_excerpt();?>
				</a>
			</div>
			<?php endwhile; ?>
			<div class="col-xs-12 borda">
				<a href="<?php echo esc_url(get_category_link( $catpadrao ));?>" class="bt-laranja bt-p">TODAS AS NOTÍCIAS</a>
				<hr/>
			</div>
		</div>
	