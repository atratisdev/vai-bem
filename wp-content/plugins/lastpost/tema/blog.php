<div class="col-md-12 ft2">
	<h3><?php echo $titulo;?></h3>
</div>
<div class="col-md-9">
	<div class="row">
		<?php $i=0; while (have_posts()) : the_post(); $imagem = get_the_post_thumbnail(get_the_ID()); ?>
			<?php if($i==0){ ?>
			<div class="col-sm-6 col-md-7 ft2">
				<article class="destaque">
					<?php 
						if(has_post_thumbnail(get_the_ID())){
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_large' );
							$url = $thumb['0'];
					?>									
						<div class="artigo__imagem" style="background-image: url('<?php echo $url;?>');"></div>
					<?php }else{ ?>
						<div class="artigo__imagem" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/semfoto.png');"></div>
					<?php } ?>
					<div class="artigo__conteudo">
						<h4 class="ft1"><?php the_title();?></h4>
						<div class="artigo__meta ft2">
							<span class="artigo__data"><?php echo get_the_date( 'd/m/Y' ); ?></span>
							<?php 
								$categories = get_the_category();							
								foreach($categories as $category){
							?> 
							<a href="<?php echo esc_url( get_category_link( $category->term_id ) );?>" class="artigo__categoria"><?php echo $category->name; ?></a>
							<?php 
								} 
							?>
						</div>
						<a class="artigo__continue ft2" href="<?php the_permalink();?>">CONTINUE LENDO</a>
					</div>
				</article>
			</div>
			<?php } $i++; endwhile; ?>
		
		<div class="col-sm-6 col-md-5">
			<?php $i=0; while (have_posts()) : the_post(); $imagem = get_the_post_thumbnail(get_the_ID()); if($i>0){?>
			<article>
				<div class="artigo__content">
					<h4 class="ft1"><?php the_title();?></h4>
					<div class="artigo__meta ft2">
						<span class="artigo__data"><?php echo get_the_date( 'd/m/Y' ); ?></span>
						<?php 
							$categories = get_the_category(); 
							foreach($categories as $category){
						?> 
						<a href="<?php echo esc_url( get_category_link( $category->term_id ) );?>" class="artigo__categoria"><?php echo $category->name; ?></a>
						<?php 
							} 
						?>
					</div>
					<a class="artigo__continue ft2" href="<?php the_permalink();?>">CONTINUE LENDO</a>
				</div>
			</article>
			<?php } $i++; endwhile; ?>
		</div>
		<div class="col-xs-12">
			<a class="btn--padrao" href="<?php echo esc_url(get_category_link( $catpadrao ));?>">VISITE MEU BLOG <i class="fa fa-arrow-right"></i></a>
		</div>
	</div>
</div>	