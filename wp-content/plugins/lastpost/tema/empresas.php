<div class="col-md-7 col-lg-8">
	<h3 class="ft2"><?php echo $titulo;?></h3>
	<div id="carousel-empresas" class="carousel slide" data-ride="carousel" data-interval="false">
		<div class="carousel-inner" role="listbox">
			<?php 
				$i=0; 
				$j=0; 
				global $wp_query; 
				$num = $wp_query->found_posts;
				while (have_posts()) {  
					the_post(); 
					global $post;
			?>
				<?php if($i==0){ if($j==0){$pri='active';}else{$pri="";}?><div class="item <?php echo $pri;?>"><div class="row"><?php } ?>
						<div class="col-sm-6 empresa">
							<div class="row">
								<div class="col-xs-4">
									<?php the_post_thumbnail( "medium", array("class"=>"livro__imagem") ); ?>
								</div>
								<div class="col-xs-8 ft2">
									<h4 class="ft1"><strong><?php the_title();?></strong></h4>					
									<p><?php echo substr(strip_tags($post->post_content), 0, 120)."...";?></p>
									<a href="<?php the_permalink();?>" class="btn--sec">Saiba Mais</a>
								</div>
							</div>
						</div>
			<?php $i++; $j++;  ?>
			<?php if($i==2 || $num==$j){ ?></div></div> <?php $i=0;} } ?>
		</div>
	
		<a class="left carousel-control" href="#carousel-empresas" role="button" data-slide="prev">
			<i class="fa fa-arrow-left" aria-hidden="true"></i>
		</a>
		<a class="right carousel-control" href="#carousel-empresas" role="button" data-slide="next">
			<i class="fa fa-arrow-right" aria-hidden="true"></i>
		</a>
	</div>
</div>