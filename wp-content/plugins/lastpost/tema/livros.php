<div class="col-md-10 ft2">
	<h3><?php echo $titulo;?></h3>
</div>
<div class="col-md-2">
	<a class="btn--padrao" href="<?php echo esc_url(get_category_link( $catpadrao ));?>">VEJA TODOS <i class="fa fa-arrow-right"></i></a>
</div>
<div class="col-xs-12">
	<div id="carousel-livros" class="carousel slide" data-ride="carousel" data-interval="false">
		<ol class="carousel-indicators">
			<?php 
				$l=0; $k=0;
				while (have_posts()) { 
					the_post(); if($k==0){ $cl="active";}else{$cl="";} 
					if($l==0){
			?>
					<li data-target="#carousel-livros" data-slide-to="<?php echo $k;?>" class="<?php echo $cl;?>"></li>
			<?php  
					}
					$l++;
					if($l==2){ $l=0; $k++;}
				} 
			?>
		</ol>
	
		<div class="carousel-inner" role="listbox">
			<?php 
				$i=0; 
				$j=0; 
				global $wp_query; 
				$num = $wp_query->found_posts;
				while (have_posts()) { 
					the_post(); 
			?>
				<?php if($i==0){ if($j==0){$pri='active';}else{$pri="";}?><div class="item <?php echo $pri;?>"><div class="row"><?php } ?>
						<div class="col-sm-6 livro">
							<div class="row">
								<div class="col-xs-4">
									<?php the_post_thumbnail( "medium", array("class"=>"livro__imagem") ); ?>
								</div>
								<div class="col-xs-8 ft2">
									<h4 class="ft1"><?php the_title();?></h4>
									<?php 
										$categories = get_the_category(); 
										foreach($categories as $category){
									?> 
										<span class="<?php echo $category->slug;?>"><?php echo $category->name; ?></span>
									<?php 
										} 
									?>						
									<p><?php the_excerpt();?></p>
									<a href="<?php the_permalink();?>" class="btn--sec">Saiba Mais</a>
									<?php if(get_field('link-compra')){?>
										<a target="_blank" title="<?php the_title();?>" href="<?php echo get_field('link-compra');?>" class="btn--pri">Comprar</a>
									<?php } ?>
								</div>
							</div>
						</div>
			<?php $i++; $j++;  ?>
			<?php if($i==2 || $num==$j){ ?></div></div> <?php $i=0;} } ?>
		</div>
	
		<!-- <a class="left carousel-control" href="#carousel-livros" role="button" data-slide="prev">
			<i class="fa fa-arrow-left" aria-hidden="true"></i>
		</a>
		<a class="right carousel-control" href="#carousel-livros" role="button" data-slide="next">
			<i class="fa fa-arrow-right" aria-hidden="true"></i>
		</a> -->
	</div>
</div>