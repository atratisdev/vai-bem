<?php
/*
Plugin Name: LastPost
Description: This plugin was made to allow users get the latest posts from a category.
Version: 1.0
Author: Carlos C. B. Alves
Author URI: http://wwww.novaerapro.com.br/ccbalves
License: GPL2
*/

/*  Copyright 2014  LastPost clonedezoito@gmail.com
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

error_reporting(E_ALL); 
ini_set('display_errors',0);
class LastpostWidget extends WP_Widget {

	public function LastpostWidget()
	{
            parent::WP_Widget(false, $name = 'Lastpost');
	}

	public function widget($args,$instance)
	{
		extract( $args );
		$titulo 		= esc_attr($instance['titulo']);
		$catpadrao		= esc_attr($instance['catpadrao']);
		$ordemresult	= esc_attr($instance['ordemresult']);	
		$quantidade 	= esc_attr($instance['quantidade']);
		$classe 		= esc_attr($instance['classe']);	
		$tema	 		= esc_attr($instance['tema']);
		$tipo	 		= esc_attr($instance['tipo']);
		$selpage    	= esc_attr($instance['selpage']);

		$tipo = "&post_type=".$tipo;
		
		if($tipo=="&post_type=page"){
			if ($ordemresult == 'crescente'){
				$order = "order=ASC&orderby=id";
			} else if($ordemresult == 'decrescente'){
				$order = "order=DESC&orderby=id";
			} else{
				$order = "orderby=count";
			}
			$query = new WP_Query( array( 'post_type' => 'page', 'post__in' => array($selpage) ) );		
		}else{
			if ($ordemresult == 'crescente'){
				$order = "order=ASC&orderby=id";
			} else if($ordemresult == 'decrescente'){
				$order = "order=DESC&orderby=id";
			} else{
				$order = "orderby=count";
			}			
			$order = "&".$order."&posts_per_page=".$quantidade;
			query_posts('cat='.$catpadrao.$tipo.$order);
		}
		require(WP_PLUGIN_DIR."/lastpost/tema/".$tema.".php");
		wp_reset_query();
		wp_reset_postdata();	 
	}

	public function update($new_instance, $old_instance){
		$instance = $new_instance;
		$instance['catpadrao'] = implode(",",$new_instance['catpadrao']);
		return $instance;
	}

	public function form($instance){
		$titulo 	= esc_attr($instance['titulo']);
        $tipo	 	= esc_attr($instance['tipo']);
		$catpadrao	= esc_attr($instance['catpadrao']);
		$ordemresult= esc_attr($instance['ordemresult']);	
		$quantidade = esc_attr($instance['quantidade']);
		$classe 	= esc_attr($instance['classe']);
		$tema	 	= esc_attr($instance['tema']);
        $temapage   = esc_attr($instance['temapage']);
        $tipo	 	= esc_attr($instance['tipo']);
        $selpage    = esc_attr($instance['selpage']);
		$categories = get_terms( 'category', 'hide_empty=0' );
		?>
                <script>
                    jQuery(document).ready(function(){
                        var item =  "#<?php echo $this->get_field_id('tipo') ?>";
                        var tiop = "<?php echo $tipo; ?>";
                        if(tiop=="page"){
                            jQuery(".post").hide();
                            jQuery(".page").show();
                        }else{
                            jQuery(".page").hide();
                            jQuery(".post").show();
                        }
                        jQuery(item).change(function() {
                            var valor = jQuery(this).val();
                            if(valor=="page"){
                                jQuery(".page").show();
                                jQuery(".post").hide();
                            }else{
                                jQuery(".page").hide();
                                jQuery(".post").show();
                            }    
                        });
                    });
                </script>    
		<p>
                    <label for="<?php echo $this->get_field_id('titulo') ?>">
                            Título no topo da lista
                    </label>
                    <input class="widefat" id="<?php echo $this->get_field_id('titulo') ?>" name="<?php echo $this->get_field_name('titulo'); ?>" type="text" value="<?php echo $titulo; ?>" />
		</p>		
		<p>
                    <label for="<?php echo $this->get_field_id('classe') ?>">
                            Classe do elemento
                    </label>
                    <input class="widefat" id="<?php echo $this->get_field_id('classe') ?>" name="<?php echo $this->get_field_name('classe'); ?>" type="text" value="<?php echo $classe; ?>" />
		</p>
                <p>
                    <label for="<?php echo $this->get_field_id('tipo') ?>">
                            Tipo de Conteúdo
                    </label>
                    <select id="<?php echo $this->get_field_id('tipo') ?>" name="<?php echo $this->get_field_name('tipo'); ?>">
                        <option <?php if ($tipo == 'post') echo 'selected'; ?> value="post">Post</option>
                        <option <?php if ($tipo == 'page') echo 'selected'; ?> value="page">Page</option>
                    </select>			
		</p>
                <p class="post">
                    <label for="<?php echo $this->get_field_id('tema') ?>">
                            Tema da Widget
                    </label>
                    <select id="<?php echo $this->get_field_id('tema') ?>" name="<?php echo $this->get_field_name('tema'); ?>">
						<option <?php if ($tema == 'default') echo 'selected'; ?> value="default">Padrão</option>
						<option <?php if ($tema == 'blog') echo 'selected'; ?> value="blog">blog</option>
						<option <?php if ($tema == 'novidades') echo 'selected'; ?> value="novidades">novidades</option>
						<option <?php if ($tema == 'empresa') echo 'selected'; ?> value="empresa">empresa</option>
						<option <?php if ($tema == 'empresas') echo 'selected'; ?> value="empresas">empresas</option>
						<option <?php if ($tema == 'livros') echo 'selected'; ?> value="livros">livros</option>
                    </select>			
		</p>
                <p class="page">
                    <label for="<?php echo $this->get_field_id('temapage') ?>">
                            Tema da Widget
                    </label>
                    <select id="<?php echo $this->get_field_id('temapage') ?>" name="<?php echo $this->get_field_name('temapage'); ?>">
                            <option <?php if ($temapage == 'default') echo 'selected'; ?> value="default">Padrão</option>
                            <option <?php if ($temapage == 'entrevistas') echo 'selected'; ?> value="entrevistas">Entrevistas</option>
                    </select>			
		</p>
                <p class="page">
                    <label for="<?php echo $this->get_field_id('selpage') ?>">
                            Selecione a Página
                    </label>
                    <select id="<?php echo $this->get_field_id('selpage') ?>" name="<?php echo $this->get_field_name('selpage'); ?>">
                    <?php  
                        $pages = get_pages(); 
                        foreach ( $pages as $page ) {
							 if($selpage==$page->ID){ $selected = "selected='selected'";}else{$selected = "";}
                              $option = '<option '.$selected.' value="' . $page->ID . '">';
                              $option .= $page->post_title;
                              $option .= '</option>';
                              echo $option;
                        } 
                    ?>
                    </select>			
		</p>
		<p class="post">
                    <label for="<?php echo $this->get_field_id('ordemresult') ?>">
                            Ordem das Itens
                    </label>
                    <select id="<?php echo $this->get_field_id('ordemresult') ?>" name="<?php echo $this->get_field_name('ordemresult'); ?>">
                            <option <?php if ($ordemresult == 'crescente') echo 'selected'; ?> value="crescente">Crescente</option>
                            <option <?php if ($ordemresult == 'decrescente') echo 'selected'; ?> value="decrescente">Decrescente</option>
                            <option <?php if ($ordemresult == 'aleatoria') echo 'selected'; ?> value="aleatoria">Aleatória</option>
                    </select>			
		</p>		
		<p class="post">
                    <label for="<?php echo $this->get_field_id('quantidade') ?>">
                            Qtd. para Exibir
                    </label>
                    <select id="<?php echo $this->get_field_id('quantidade') ?>" name="<?php echo $this->get_field_name('quantidade'); ?>">
                            <option <?php if ($quantidade == 1) echo 'selected'; ?> value="1">1</option>
                            <option <?php if ($quantidade == 2) echo 'selected'; ?> value="2">2</option>
                            <option <?php if ($quantidade == 3) echo 'selected'; ?> value="3">3</option>
                            <option <?php if ($quantidade == 4) echo 'selected'; ?> value="4">4</option>
                            <option <?php if ($quantidade == 5) echo 'selected'; ?> value="5">5</option>
                            <option <?php if ($quantidade == 6) echo 'selected'; ?> value="6">6</option>
                            <option <?php if ($quantidade == 7) echo 'selected'; ?> value="7">7</option>
                            <option <?php if ($quantidade == 8) echo 'selected'; ?> value="8">8</option>
                            <option <?php if ($quantidade == 9) echo 'selected'; ?> value="9">9</option>
                            <option <?php if ($quantidade == 10) echo 'selected'; ?> value="10">10</option>
                    </select>			
                </p>
		<p class="post"> 
                    <label for="<?php echo $this->get_field_id('catpadrao') ?>">
                            Categoria Padrão
                    </label>
                    <select multiple="multiple" id="<?php echo $this->get_field_id('catpadrao') ?>" name="<?php echo $this->get_field_name('catpadrao'); ?>[]">
                        <?php 
                            foreach($categories as $category){
                                $cares = explode(",",$catpadrao);
                        ?>		
                            <option <?php if (in_array($category->term_id,$cares)){  echo 'selected';} ?> value='<?php echo $category->term_id;?>'><?php echo $category->name;?></option>
                        <?php } ?>
                    </select>					
		</p>		
		
        <?php 
	}
}

// register it
add_action('widgets_init', create_function('', 'return register_widget("LastpostWidget");'));
?>