<?php
/*
Plugin Name: Opçoes
Description: Plugin para configurar detalhes no tema.
Version: 1.0
Author: Carlos C. B. Alves
Author URI: http://wwww.atratis.com.br/
License: GPL2
*/

/*  Copyright 2016  Opçoes carlosalves@atratis.com.br
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//error_reporting(E_ALL); 
//ini_set('display_errors', 1);

add_action('admin_menu', 'menuopcoes');
function menuopcoes() {
	add_menu_page('Opções do Tema', 'Configurações Gerais', 'administrator', __FILE__, 'opcoes_settings_page' , plugins_url('/images/icon.png', __FILE__) );
	add_action( 'admin_init', 'register_opcoes_settings' );
}

function register_opcoes_settings() {
	register_setting( 'opcoes-settings-group', 'requsertoken' );
	register_setting( 'opcoes-settings-group', 'emailpismo' );
	register_setting( 'opcoes-settings-group', 'senhapismo' );
	register_setting( 'opcoes-settings-group', 'linkpismo' );	
	register_setting( 'opcoes-settings-group', 'funccadcliente' );
	register_setting( 'opcoes-settings-group', 'funcsearchplace' );
	register_setting( 'opcoes-settings-group', 'codanalytics' );
	register_setting( 'opcoes-settings-group', 'linkadquiracartao' );
	register_setting( 'opcoes-settings-group', 'linkredecredenciada' );
}

function opcoes_settings_page() {
?>
<div class="wrap">
	<h2>Opções do Vai Bem</h2>
	<form method="post" action="options.php">
		<h2>Pismo</h2>
		<?php settings_fields( 'opcoes-settings-group' ); ?>
		<?php do_settings_sections( 'opcoes-settings-group' ); ?>
		<table class="form-table">	
			<tr valign="top">
				<th scope="row">Link para Aquisição do Token</th>
				<td><input type="text" name="requsertoken" value="<?php echo esc_attr( get_option('requsertoken') ); ?>" /></td>
			</tr>					
			<tr valign="top">
				<th scope="row">Email para Login no Pismo</th>
				<td><input type="text" name="emailpismo" value="<?php echo esc_attr( get_option('emailpismo') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Senha para Login no Pismo</th>
				<td><input type="text" name="senhapismo" value="<?php echo esc_attr( get_option('senhapismo') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Link do Pismo - Entrada</th>
				<td><input type="text" name="linkpismo" value="<?php echo esc_attr( get_option('linkpismo') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Método - Cadastro do Cliente</th>
				<td><input type="text" name="funccadcliente" value="<?php echo esc_attr( get_option('funccadcliente') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Método - Lista de Estabelecimentos</th>
				<td><input type="text" name="funcsearchplace" value="<?php echo esc_attr( get_option('funcsearchplace') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Link - Adquira seu Cartão Vai Bem</th>
				<td><input type="text" name="linkadquiracartao" value="<?php echo esc_attr( get_option('linkadquiracartao') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Link - Rede Credencidada</th>
				<td><input type="text" name="linkredecredenciada" value="<?php echo esc_attr( get_option('linkredecredenciada') ); ?>" /></td>
			</tr>			
		</table>
		<table class="form-table">
			<h2>Tema</h2>
			<tr valign="top">
				<th scope="row">Código Analytics</th>
				<td><textarea name="codanalytics" ><?php echo esc_attr( get_option('codanalytics') ); ?></textarea></td>
			</tr>						
		</table>
		<?php submit_button(); ?>
	</form>
</div>
<?php } ?>