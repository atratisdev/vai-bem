<?php
/*
 * Show Banners
 * Description: Adds a widget that display banners of a category.
 * Author: Carlos C. B. Alves	
 * Version: 1.0
*/ 

class BannersWidget extends WP_Widget {

	public function BannersWidget(){
		parent::WP_Widget(false, $name = 'Banners');
	}

	public function widget($args,$instance)
	{

		extract( $args );
		$exibirtitulo   = esc_attr($instance['exibirtitulo']);	
		$titulo 		= esc_attr($instance['titulo']);
		$classe 		= esc_attr($instance['classe']);
		$alvoban 		= esc_attr($instance['alvoban']);
		$quantidade		= esc_attr($instance['quantidade']);
		$ordemresult	= esc_attr($instance['ordemresult']);
		$tema    		= esc_attr($instance['tema']);
		$catpadrao		= esc_attr($instance['catpadrao']);
		
		if ($ordemresult == 'crescente'){ 
			$order = "order=ASC&orderby=id"; 
		} else if($ordemresult == 'decrescente'){ 
			$order = "order=DESC&orderby=id";
		} else{ 
			$order = "orderby=count";
		}
		$categories = get_terms( 'categoria', $order.'&hide_empty=1&parent='.$catpadrao );
	   	$resultsgeral = $this->comando($catpadrao,$quantidade);
		require(WP_PLUGIN_DIR."/banners/temas/".$tema.".php");
       
	}
    
    function comando($catpadrao,$quantidade){
        global $wpdb;
        
		$tbl_name = $wpdb->prefix .'posts';		
		$tbl_name2 = $wpdb->prefix .'postmeta';		
		$tbl_name3 = $wpdb->prefix .'term_taxonomy';
		$tbl_name4 = $wpdb->prefix .'term_relationships';		
        
		$resultsgeral = $wpdb->get_results("SELECT * FROM
                                                    ".$tbl_name." as p,
                                                    ".$tbl_name2." as m,
                                                    ".$tbl_name3." as tt,
                                                    ".$tbl_name4." as rt
                                                    WHERE p.ID = m.post_id
                                                    AND p.post_type = 'banners'
                                                    AND p.post_status = 'publish'
                                                    AND tt.term_id = ".$catpadrao."
                                                    AND tt.term_taxonomy_id=rt.term_taxonomy_id
                                                    AND rt.object_id = p.id
                                                    GROUP BY p.id
                                                    ORDER BY p.id ASC limit ".$quantidade); 
		$wpdb->flush();											
        return $resultsgeral;
    }

	public function update($new_instance, $old_instance){
            $instance = $new_instance;
            return $instance;
	}

	public function form($instance){
		
		$exibirtitulo   = esc_attr($instance['exibirtitulo']);	
		$titulo 	= esc_attr($instance['titulo']);
		$classe 	= esc_attr($instance['classe']);
		$alvoban 	= esc_attr($instance['alvoban']);
		$quantidade	= esc_attr($instance['quantidade']);
		$ordemresult	= esc_attr($instance['ordemresult']);
		$tema    	= esc_attr($instance['tema']);
		$catpadrao	= esc_attr($instance['catpadrao']);
		
		
		$categories = get_terms( 'categoria', 'hide_empty=0' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id('exibirtitulo') ?>">
				Exibir Título
			</label>
			<select id="<?php echo $this->get_field_id('exibirtitulo') ?>" name="<?php echo $this->get_field_name('exibirtitulo'); ?>">
				<option <?php if ($exibirtitulo == '0') echo 'selected'; ?> value="0">Não</option>
				<option <?php if ($exibirtitulo == '1') echo 'selected'; ?> value="1">Sim</option>
			</select>			
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('titulo') ?>">
				Título no topo da lista
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id('titulo') ?>" name="<?php echo $this->get_field_name('titulo'); ?>" type="text" value="<?php echo $titulo; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('classe') ?>">
				Classe do elemento
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id('classe') ?>" name="<?php echo $this->get_field_name('classe'); ?>" type="text" value="<?php echo $classe; ?>" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('alvoban') ?>">
				Alvo dos Banners
			</label>
			<select id="<?php echo $this->get_field_id('alvoban') ?>" name="<?php echo $this->get_field_name('alvoban'); ?>">
				<option <?php if ($alvoban == '_self') echo 'selected'; ?> value="_self">Mesma Página</option>
				<option <?php if ($alvoban == '_blank') echo 'selected'; ?> value="_blank">Nova Página</option>
			</select>			
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('quantidade') ?>">
				Quantidade para mostrar
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id('quantidade') ?>" name="<?php echo $this->get_field_name('quantidade'); ?>" type="text" value="<?php echo $quantidade; ?>" />
		</p>		

		<p>
			<label for="<?php echo $this->get_field_id('ordemresult') ?>">
				Ordem das categorias
			</label>
			<select id="<?php echo $this->get_field_id('ordemresult') ?>" name="<?php echo $this->get_field_name('ordemresult'); ?>">
				<option <?php if ($ordemresult == 'crescente') echo 'selected'; ?> value="crescente">Crescente</option>
				<option <?php if ($ordemresult == 'decrescente') echo 'selected'; ?> value="decrescente">Decrescente</option>
				<option <?php if ($ordemresult == 'aleatoria') echo 'selected'; ?> value="aleatoria">Aleatória</option>
			</select>			
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('tema') ?>">
				Tema da Widget
			</label>
			<select id="<?php echo $this->get_field_id('tema') ?>" name="<?php echo $this->get_field_name('tema'); ?>">
				<option <?php if ($tema == 'padrao') echo 'selected'; ?> value="padrao">Padrão</option>
				<option <?php if ($tema == 'destaque') echo 'selected'; ?> value="destaque">Destaque</option>
				<option <?php if ($tema == 'destaquemob') echo 'selected'; ?> value="destaquemob">Destaque Mobile</option>
				<option <?php if ($tema == 'vaibemporque') echo 'selected'; ?> value="vaibemporque">Vai Bem Porque</option>
				<option <?php if ($tema == 'vaibemfunciona') echo 'selected'; ?> value="vaibemfunciona">Vai Bem Funciona</option>
				<option <?php if ($tema == 'vaibemvideos') echo 'selected'; ?> value="vaibemvideos">Vai Bem Videos</option>
                <option <?php if ($tema == 'redesocial') echo 'selected'; ?> value="redesocial">Redes Sociais</option>
			</select>			
		</p>                
						

		<p>
			<label for="<?php echo $this->get_field_id('catpadrao') ?>">
				Categoria Padrão
			</label>
			<select id="<?php echo $this->get_field_id('catpadrao') ?>" name="<?php echo $this->get_field_name('catpadrao'); ?>">
				<?php 
					foreach($categories as $category){
			?>		
					<option <?php if ($catpadrao == $category->term_id) echo 'selected'; ?> value='<?php echo $category->term_id;?>'><?php echo $category->name;?></option>
			<?php
					}	
				?>
			</select>					
		</p>
        <?php 
	}
}

// register it
add_action('widgets_init', create_function('', 'return register_widget("BannersWidget");'));
