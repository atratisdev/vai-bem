<?php
/*
Plugin Name: Banners
Description: This plugin was made to allow users put banners on their website.
Version: 1.0
Author: Carlos C. B. Alves
Author URI: http://wwww.novaerapro.com.br/ccbalves
License: GPL2
*/

/*  Copyright 2012  Banners clonedezoito@gmail.com
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
include_once("widgetBanners.php");
//error_reporting(E_ALL); 
//ini_set('display_errors', 1);
class banners {
	private static $wpdb;
	
	//inicializa o plugin 
	public function banners_register() {
		if (is_admin()) {
			if($_GET['post_type']=='banners'){
				wp_register_script('transicao_efeito',WP_PLUGIN_URL.'/banners/js/lebanners.js', array(), '1.0' );
				wp_enqueue_script('transicao_efeito');		
			}
		}
		
		  $labels = array(
			'name' => _x( 'Categorias', 'categorias dos banners' ),
			'singular_name' => _x( 'Categoria', 'categoria de banners especifica' ),
			'search_items' =>  __( 'Pesquisar Categoria' ),
			'all_items' => __( 'Todas Categorias' ),
			'parent_item' => __( 'Categoria Pai' ),
			'parent_item_colon' => __( 'Categoria Pai:' ),
			'edit_item' => __( 'Editar Categoria' ), 
			'update_item' => __( 'Atualizar Categoria' ),
			'add_new_item' => __( 'Salvar Categoria' ),
			'new_item_name' => __( 'Nome da Categoria' ),
			'menu_name' => __( 'Categoria' ),
		  ); 	

		  register_taxonomy('categoria',array('banners'), array(
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'categoria' ),
		  ));		
		
		add_rewrite_tag('%banner%','([^&]+)');
		add_rewrite_rule('^banner/([^/]*)/?', '?banner=$matches[1]','top');
		
		$labels = array(
			'name' => _x('Banners', 'detalhes dos banners'),
			'singular_name' => _x('Banner', 'detalhes do banner'),
			'add_new' => _x('Adicionar novo', 'novo banner'),
			'add_new_item' => __('Adicionar Novo Banner'),
			'edit_item' => __('Editar Banner'),
			'new_item' => __('Novo Banner'),
			'view_item' => __('Ver dados do banner'),
			'search_items' => __('Procurar Banner'),
			'not_found' =>  __('Nada encontrado'),
			'not_found_in_trash' => __('Nada encontrado na lixeira'),
			'parent_item_colon' => ''
		);
	  
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
			'capability_type' => 'post',
			'comments'=> true,
			'revisions' =>true,
			'taxonomies' => array( 'categoria', 'categoria '),
			'menu_icon' => WP_PLUGIN_URL . '/banners/img/hive.gif',  // caminho do ícone
			'hierarchical' => false,
			'menu_position' => null,
			'has_archive' => true,
			'supports' => array('title','editor','excerpt','thumbnail','comments'),
			'rewrite' => array('slug' => 'banners')
		  );
	 
		register_post_type('banners',$args);
		flush_rewrite_rules();
	}

	public function admin_init(){
		add_meta_box("ordem_meta", "Ordem", array("banners", "ordem"), "banners", "side", "low");
		add_meta_box("bannerurl_meta", "Link do Banner", array("banners", "bannerurl"), "banners", "side", "low");
		add_meta_box("codvideo_meta", "Código do Vídeo", array("banners", "codvideo"), "banners", "side", "low");
		add_meta_box("subtitulo_meta", "Subtítulo", array("banners", "subtitulo"), "banners", "normal", "high");
	}
	 
	public function ordem(){
	  global $post;
	  $custom = get_post_custom($post->ID);
	  $ordem = $custom["ordem"][0];
	  ?>
		<label>Ordem</label>
		<input type="text" onkeypress='return SomenteNumero(event)' name="ordem" value="<?php echo $ordem; ?>" />
		<script type='text/javaScript'>
		function SomenteNumero(e){
			var tecla=(window.event)?event.keyCode:e.which;   
			if((tecla>47 && tecla<58)) return true;
			else{
				if (tecla==8 || tecla==0) return true;
			else  return false;
			}
		}
		</script>		
	  <?php
	}
	 
	public function bannerurl(){
	  global $post;
	  $custom = get_post_custom($post->ID);
	  $bannerurl = $custom["bannerurl"][0];
	  ?>
		<label>Link do Banner</label>
		<input type="text" name="bannerurl" value="<?php echo $bannerurl;?>" />
	  <?php
	}
	
	public function codvideo(){
	  global $post;
	  $custom = get_post_custom($post->ID);
	  $codvideo = $custom["codvideo"][0];
	  ?>
		<label>Código do Vídeo</label>
		<input type="text" name="codvideo" value="<?php echo $codvideo;?>" />
	  <?php
	}

	public function subtitulo(){
	  global $post;
	  $custom = get_post_custom($post->ID);
	  $subtitulo = $custom["subtitulo"][0];
	  ?>
		<label>Subtítulo do Banner</label>
		<input type="text" name="subtitulo" value="<?php echo $subtitulo;?>" />
	  <?php
	}	

	 
	public function save_details(){
	  global $post;
	  update_post_meta($post->ID, "ordem", $_POST["ordem"]);
	  update_post_meta($post->ID, "bannerurl", $_POST["bannerurl"]);
	  update_post_meta($post->ID, "codvideo", $_POST["codvideo"]);
	  update_post_meta($post->ID, "subtitulo", $_POST["subtitulo"]);
	}


	public function banners_edit_columns($columns){
	  $columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => "Nome do Banner",
		"description" => "Descrição",
		"bannerurl" => "Link do Banner",
	  );
	 
	  return $columns;
	}
	
	public function banners_custom_columns($column){
	  global $post;
	 
	  switch ($column) {
		case "description":
		  the_excerpt();
		  break;
		case "bannerurl":
		  $custom = get_post_custom();
		  echo $custom["bannerurl"][0];
		  break;
	  }
	}
	
	//cria banco de insere configurações padrão
	function desinstalar(){
		global $wpdb;
		$tbl_name = $wpdb->prefix .'posts';
		$query = "DELETE FROM `".$tbl_name."` WHERE post_type='banners'";
		$wpdb->show_errors();
		return $wpdb->query($query);
	}
}

add_filter('init', array('banners','banners_register'));
add_action('save_post', array('banners','save_details'));
add_action("admin_init", array('banners',"admin_init"));
add_action("manage_posts_custom_column", array('banners',"banners_custom_columns"));
add_filter("manage_edit-banners_columns", array('banners',"banners_edit_columns"));
register_deactivation_hook(__FILE__,array('banners','desinstalar'));
?>