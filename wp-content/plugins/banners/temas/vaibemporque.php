<?php $linknovocartao = get_option("linkadquiracartao");?>
<div class="container <?php if($classe){echo $classe;}?>">
	<?php if($exibirtitulo==1){ ?>
	<div class="row">
		<div class="col-xs-12">        
			<h2><strong>VAI BEM</strong> <?php echo $titulo;?></h2>
		</div>
	</div>
	<?php } ?>
	<div class="row">   
		
	<?php 
		$i=0;
		foreach($resultsgeral as $result){
    		$imagem = get_the_post_thumbnail($result->ID);
    		$link = get_post_meta($result->ID, 'bannerurl', true);
			if($i<3){
	?>
		<div class="col-md-4 col-xs-12">        
			<div class="item">
				<div class="box_img wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
					<?php echo $imagem;?>
				</div>
				<div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
					<h3><?php echo $result->post_title; ?></h3>
					<p><?php echo $result->post_content; ?></p>
				</div>
			</div>
		</div>
	<?php }$i++;} ?>

	</div>
	<div class="row">
		<div class="col-xs-12 col-md-2"></div>

		<?php
			$i=0;
			foreach($resultsgeral as $result){
	    		$imagem = get_the_post_thumbnail($result->ID);
	    		$link = get_post_meta($result->ID, 'bannerurl', true);
				if($i>2){
		?>
		<div class="col-md-4 col-xs-12">
			<div class="item">
				<!-- a href="<?php echo $link;?>" -->
					<div class="box_img  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
						<?php echo $imagem;?>
					</div>
					<div class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
						<h3><?php echo $result->post_title; ?></h3>
						<p><?php echo $result->post_content; ?></p>
					</div>
				<!-- /a -->
			</div>
		</div>
		<?php }$i++;} ?> 
		
		<div class="col-xs-12 col-md-2"></div>      
	</div>
	<div class="row">
		<div class="col-xs-12">
			<a target="_blank" href="<?php echo $linknovocartao;?>" class="bt_padrao">Adquira o seu Cartão Vai Bem</a>
			<span class="cf">*Cônjuge e filhos</span>
		</div>
	</div>
</div>