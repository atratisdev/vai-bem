<?php $linknovocartao = get_option("linkadquiracartao");?>
<div class="container <?php if($classe){echo $classe;}?>">
	<?php if($exibirtitulo==1){ ?>
	<div class="row">
		<div class="col-xs-12">    
			<div class="seta_funciona"><i class="fa fa-chevron-down u-animation--pulse" aria-hidden="true"></i></div>    
			<h2><strong>VAI BEM</strong> <?php echo $titulo;?></h2>
		</div>
	</div>
	<?php } ?>
	
	<?php 
		$j=1;
		foreach($resultsgeral as $result){
    		$imagem = get_the_post_thumbnail($result->ID);
    		$link = get_post_meta($result->ID, 'bannerurl', true);
			if($j==1){
	?>
	<div class="row">
		<div class="col-xs-12 col-md-1"></div>			
	<?php } ?>	
		<div class="col-md-4 col-xs-12">        
			<!-- a target="<?php echo $alvoban; ?>" href="<?php echo $link;?>" class="item" -->
				<div class="box_img <?php echo $result->post_name;?>">
					<?php echo $imagem;?>
				</div>
				<p><?php echo $result->post_content; ?></p>
			<!-- >/a -->
		</div>

		<?php $j++; ?>
	
		<?php if($j==2){ ?>
			<div class="col-xs-12 col-md-2"></div>
		<?php } ?>
		
		<?php if($j>2){ $j=1;?>
			<div class="col-xs-12 col-md-1"></div>
		</div>
		<?php } ?>
		
	<?php } ?> 	
		
	<div class="row">
		<div class="col-xs-12">
			<a target="_blank" href="<?php echo $linknovocartao;?>" class="bt_padrao">Adquira o seu Cartão Vai Bem  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		</div>
	</div>

</div>