<div class="container <?php if($classe){echo $classe;}?>">
	<div class="row">
		<div class="col-xs-12">
			<div class="cycle-slideshow" data-cycle-swipe=true data-cycle-log="false" data-cycle-timeout=0 data-cycle-swipe-fx=scrollHorz data-cycle-slides="li">
				<div class="cycle-pager"></div>
				<ul class="row" >
					<?php 
						$m=0;
						foreach($resultsgeral as $result){
				    		$imagem 	= get_the_post_thumbnail($result->ID);
				    		$link 		= get_post_meta($result->ID, 'bannerurl', true);
							$codvideo   = get_post_meta($result->ID, 'codvideo', true);
							$subtitulo  = get_post_meta($result->ID, 'subtitulo', true);
							if($m==0){
								$clk = "wow fadeInLeft"; 
								$lks = "wow fadeInRight";
								$lko = 'data-wow-duration="1s" data-wow-delay="0.5s"';
								$kso = 'data-wow-duration="1s" data-wow-delay="0.5s"';
							}else{
								$clk = ""; 
								$lks = "";
								$lko = "";
								$kso = "";
							}
					?>				
					<li>
						<div class="col-md-7 <?php echo $clk;?> " <?php echo $lko;?> >
							<span class="monitor">
								<iframe width="533" height="300" src="https://www.youtube.com/embed/<?php echo $codvideo ;?>?html5=1&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
							</span>
						</div>
						<div class="col-md-5 <?php echo $lks;?> " <?php echo $kso;?> >
							<h2><?php echo $result->post_title; ?></h2>
							<h3><?php echo $subtitulo;?></h3>
							<p><?php echo $result->post_content; ?></p>
						</div>
					</li>
					<?php 
						$m++; }
					?>
				</ul>
			</div>
		</div>
	</div>
</div>