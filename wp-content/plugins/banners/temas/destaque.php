<div id="banner-topo" class="carousel slide <?php if($classe){echo $classe;}?>" data-ride="carousel">
	<ol class="carousel-indicators">
		<?php $j=0; foreach($resultsgeral as $result){ if($j==0){ $lap = "active";}else{$lap = "";} ?>
		<li data-target="#banner-topo" data-slide-to="<?php echo $j;?>" class="<?php echo $lap;?>"></li>
		<?php $i++; } ?>		
	</ol>

	<div class="carousel-inner" role="listbox">
	    <?php 
	    	$i=0;
	        foreach($resultsgeral as $result){
	            $imagem = get_the_post_thumbnail($result->ID);
	            $url = wp_get_attachment_url( get_post_thumbnail_id($result->ID));
	            $link = get_post_meta($result->ID, 'bannerurl', true);
	            if($i==0){ $lao = "active";}else{$lao = "";}
	    ?>
		<a href="<?php echo $link;?>" class="item <?php echo $lao;?>" style="background-image: url(<?php echo $url;?>);" alt="img" title="img"></a>
		<?php 
				$i++;
			}
		?>
	</div>

	<a class="left carousel-control" href="#banner-topo" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Anterior</span>
	</a>
	<a class="right carousel-control" href="#banner-topo" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Próximo</span>
	</a>
</div>