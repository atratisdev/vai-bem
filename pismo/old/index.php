<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Pismo Login</title>
</head>
<body>
  <h1>Exemplo Pismo</h1>
  <form id="formtoken">
    <label for="email">Email</label>
    <input type="email" name="email">
    <br/>
    <label for="password">Password</label>
    <input type="password" name="password">
    <button type="submit">Enviar</button>
  </form>

  <div id="result"></div>
</body>
<script type="text/javascript">

var form = document.getElementById("formtoken");

document.addEventListener('DOMContentLoaded', function(){ 
	document.getElementById("formtoken").submit();
}, false);

form.addEventListener('submit', function(evt) {
  evt.preventDefault();

  var data = {
	  email: "carlosalves@atratis.com.br", //document.querySelector('input[name="email"]').value,
	  password: "atratis1234" //document.querySelector('input[name="password"]').value
  }

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "http://api-auth-qa.elasticbeanstalk.com/v1/users/token");
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(data));

  xhr.onreadystatechange = function(state) {
      if(xhr.readyState === XMLHttpRequest.DONE) {
        document.querySelector('#result').textContent = 'Token : ' + JSON.parse(xhr.responseText).token;
		document.querySelector('#token').value = JSON.parse(xhr.responseText).token;
		document.querySelector('#tenant').value = JSON.parse(xhr.responseText).tenant;
      }
  };
});
</script>

  <form method="post" action="http://qa.pismo.io/#/acquisition" >
    <label for="token">Token</label>
    <input type="text" name="token" id="token" />
    <br/>
    <label for="tenant">Tenant</label>
    <input type="tenant" name="tenant" id="tenant" />
    <button type="submit">Enviar</button>
  </form>


</html>
