<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'atratis_vaibem');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c4T:oIm,*FkbP2_W@nOywX6(MmJNcXXAWI4h~ku-`5<pAE}}z!T3JECj.=)].!z!');
define('SECURE_AUTH_KEY',  'n:tBHu^U*W!uyDA_[Y[eKQIIATQ|^l(OWf-=7q,Os|uqy<lp*G25hY8d?75TF%bj');
define('LOGGED_IN_KEY',    'K9bn>Wgz?20L.}%,Y]<qG]:3.eguIfQT`{+>j|#~-~gb#!AHK$:V;Z^NRE5JzU;`');
define('NONCE_KEY',        '?=9r!>Qp9%@#bn!Ttu+-@s{N6-#;UZy_%A<A5c1t}uFF|!VHf]ZMVl{ qOVoX,SV');
define('AUTH_SALT',        'I9,V`lJ)d0 EU-}kbP.]@;)6%9[i6pG`$~~p>9s74,3x_UinTMZ|#v`iL$|t-;ty');
define('SECURE_AUTH_SALT', 'tOD~#`V^N#pr912QD1V:}DvmE9:U;+l!Kx!aP6&tLud70l L,K|qKwZiZ6nc>%=w');
define('LOGGED_IN_SALT',   '8p/S`a!9/FE03g/6Q,)j0StU@T Lad1#T*Ggz>wlizk1n:?(wK(x87uIF4v.oj}Z');
define('NONCE_SALT',       'iH8<6.O2+_ncYU;mN`!/7|j6556AB}62zZwOT,KlSsJ$X/Z;,EwP{/Jk]2:m4jCi');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'fbcvb_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', true);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
