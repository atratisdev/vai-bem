<!doctype html>
<html class="no-js" lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Vai Bem</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="build/css/estilo.css">


</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<header class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <button class="toggle">
                    <span></span><span></span><span></span>
                </button>
                <div class="topo">
                    <div class="menu-topo">
                        <h1 class="marca">
                            <a href="index.php"><img src="build/images/logo.png" alt="Vai bem" /></a>
                        </h1>
                        <nav class="menu">
                            <li class="current"><a href="interna.php">Vai Bem para o cliente</a></li>
                            <li><a href="#">Vai Bem para o credenciado</a></li>
                            <li class="rede_conveniada"><a href="#">Rede Conveniada</a></li>
                        </nav>
                        <nav class="fr">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle acesse" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acesse sua área</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Para Clientes</a></li>
                                    <li><a href="#">Para Credenciados</a></li>
                                </ul>
                            </li>
                            <li><a href="#" class="adquira">Chat Online</a></li>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
