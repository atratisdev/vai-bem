// instanciando módulos
var gulp = require('gulp');
var notify = require('gulp-notify');
var changed = require('gulp-changed');
var connect = require('gulp-connect');
var livereload = require('gulp-livereload');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var imageop = require('gulp-image-optimization');

gulp.task('connect', function() {
    connect.server({ root: 'carmehil', livereload: true });
});

gulp.task('reload', function() {
    return gulp.src('./*.php')
        .pipe(connect.reload())
        .pipe(notify({ message: 'Página recarregada', onLast: true }))
});

gulp.task('sass', function() {
    return gulp.src('./assets/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', notify.onError(function(error) {
            return 'Erro ao compilar CSS: ' + error.message;
        })))
        .pipe(postcss([autoprefixer]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build/css'))
        .pipe(gulp.dest('../wp-content/themes/atratis/css/'))
        .pipe(connect.reload())
        .pipe(livereload())
        .pipe(notify({ message: 'CSS concluído', onLast: true }));
});

gulp.task('babel', function() {
    return gulp.src([        
            './assets/js/javascripts/bootstrap.min.js',
            './assets/js/javascripts/modernizr-custom.js',
            './assets/js/javascripts/wow.min.js',
            './assets/js/javascripts/menu.js'
        ])

        .pipe(sourcemaps.write('.'))
        .pipe(concat('scripts.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build/js'))
        .pipe(gulp.dest('../wp-content/themes/atratis/js'))
        .pipe(notify({ message: 'Scripts concluídos', onLast: true }));
});

gulp.task('reload-js', ['babel'], function() {
    connect.reload();
});

gulp.task('images', function() {
    return gulp.src('./assets/images/*')
        .pipe(changed('./build/images'))
        .pipe(imageop({ optimizationLevel: 5, progressive: true, interlaced: true }).on('error', notify.onError(function(error) {
            return 'Erro na otimização das imagens: ' + error.message;
        })))
        .pipe(gulp.dest('./build/images'))
        .pipe(gulp.dest('../wp-content/themes/atratis/images'))
        .pipe(notify({ message: 'Imagens otimizadas', onLast: true }));
});

gulp.task('watch', ['connect', 'sass', 'babel', 'images'], function() {
    gulp.watch('./assets/sass/**/*.scss', ['sass']);
    gulp.watch('./assets/js/**/*.js', ['reload-js']);
    gulp.watch(['./*.php'], ['reload']);
    gulp.watch(['../wp-content/themes/atratis/*.php'], ['reload']);
});

gulp.task('default', ['connect', 'sass', 'babel', 'images', 'watch']);