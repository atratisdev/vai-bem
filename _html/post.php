<?php include('header.php'); ?>

    <section class="banner_blog wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="item-blog"
             style="background-image: url(build/images/blog-banner.jpg); height: 279px; background-position: center;"
             alt="img" title="img">
        </div>
    </section>

    <section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
        <div class="item-blog">
            <img src="build/images/img-mob.jpg" alt="img" title="img">
        </div>
    </section>

    <div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">

        <div class="row">

            <div class="interna">

                <div class="breadcrumbs col-xs-12">
                    <ul>
                        <li><a href="index.php">Página inicial <i class="fa fa-chevron-circle-right"
                                                                  aria-hidden="true"></i></a></li>
                        <li><a href="index.php">Página inicial teste 2 <i class="fa fa-chevron-circle-right"
                                                                          aria-hidden="true"></i></a></li>

                        <li class="active">Notícias</li>
                    </ul>
                </div>


                <div class="col-md-9">
                    <div class="post_interna">
                        <h1 class="titulo">
                            Lorem ipsum dolor med ipsim dolor
                            otícias med
                        </h1>
                        <div class="item">
                            <div class="box_texto">
                                <div class="box-dt">
                                    <div class="data">30/05/2016</div>
                                    <div class="tags"><a href="#">Dicas</a><a href="#">Clipping</a></div>
                                </div>

                                <h2>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Sed vulputate venenatis
                                    magna, eu sagittis odio molestie sit oles
                                    tie sit amet.
                                </h2>

                                <img class="imagem" src="build/images/ft.jpg" alt="img" title="img">
                                <p>Lorem ipsum dolor sit amet, consectetur ttadipiscing elit. Mauris laoreet metus dui,
                                    non consectetur quam tristiqueO cartão Bem Mais é a solução para você que precisa
                                    lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus nec felis
                                    dignissim vulputate in vitae justo. Donec auctor nibh at purus s psumdolo odales, a
                                    frintgilla mauris cursus. <a href="">Praesent</a> malesuada tellus sapien, eget aliquet elit
                                    aliquet ac.

                                    <Br><br>
                                    Mauris congue urna non convallis luctus. Proin quis varius mi, sed molestie augue.
                                    Vivamus ornare a dui vel consectetur. Etiam luctus scelerisque pellentesque. Morbi
                                    vitae vulputate erat. <a href="">Curabitur</a> vestibulum varius dolor, id facilisis ante viverra
                                    vel.

                                    <h3>frintgilla mauris cursus. Praesent malesuada tellus sapien, eget aliquet elit
                                    aliquet ac.</h3>

                                    Donec vel cursus augue, ullamcorper pretium quam. Duis accumsan pretium luctus.
                                    C et.
                                    <Br><br>
                                    Mauris congue urna non convallis luctus. Proin quis varius mi, sed molestie augue.
                                    Vivamus

                                    <h4>frintgilla mauris cursus. Praesent malesuada tellus sapien, eget aliquet elit
                                        aliquet ac.</h4>
                                    ornare a dui vel consectetur. Etiam luctus scelerisque pellentesque. Morbi
                                    vitae vulputate erat. Curabitur vestibulum varius dolor, id facilisis ante viverra
                                    vel. Donec vel cursus augue, ullamcorper pretium quam. Duis accumsan pretium luctus.
                                    C et.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="compartilhe">

                        <p>Compartilhe: </p>

                        <div class="box_midia"><img src="build/images/midia.jpg"></div>

                    </div>

                    <div class="relacionados">

                        <h2 class="tit-relacionados">Conteúdos Relacionados</h2>

                        <div class="item">

                            <a href="#" class="box_img">
                                <div style="background-image: url('build/images/post-off.jpg');"></div>
                            </a>

                          <h3><a href="#">Pretium quam. Duis accumsan pretium luctus.</a></h3>

                            <a href="post.php" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

                        </div>
                        <div class="item">

                            <a href="#" class="box_img">
                                <div style="background-image: url('build/images/post-off.jpg');"></div>
                            </a>

                            <h3><a href="#">Pretium quam. Duis accumsan pretium luctus.</a></h3>

                            <a href="post.php" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

                        </div>
                        <div class="item">

                            <a href="#" class="box_img">
                                <div style="background-image: url('build/images/post-off.jpg');"></div>
                            </a>

                            <h3><a href="#">Pretium quam. Duis accumsan pretium luctus.</a></h3>

                            <a href="post.php" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

                        </div>



                    </div>

                    <br clear="all"/>

                </div>
                <aside class="col-md-3">
                    <div class="categoria">

                        <h2>CATEGORIAS</h2>
                        <ul>

                            <li><a href="#">Todas</a></li>
                            <li><a href="#" class="current">Casos de Sucesso</a></li>
                            <li><a href="#">Depoimento</a></li>
                            <li><a href="#">Iluminação</a></li>
                            <li><a href="#">Eventos</a></li>

                        </ul>
                    </div>

                    <section class="newsletter  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">

                        <h2>VAI BEM</h2>
                        <h3>ficar por dentro das novidades.</h3>
                        <form>
                            <div><input type="text" class="inpt inpt_nome" placeholder="Digite aqui seu nome"></div>
                            <div><input type="text" class="inpt inpt_email" placeholder="Digite aqui seu e-mail"></div>
                            <div><input type="submit" class="bt_padrao" value="Assinar"></div>
                        </form>

                    </section>

                </aside>


            </div>
        </div>
    </div>


<?php include('footer.php'); ?>