<?php include('header.php'); ?>

<section class="banner_principal wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
        
    <div id="banner-topo" class="carousel slide" data-ride="carousel">
      <!-- Indicators 
      <ol class="carousel-indicators">
        <li data-target="#banner-topo" data-slide-to="0" class="active"></li>
        <li data-target="#banner-topo" data-slide-to="1"></li>
        <li data-target="#banner-topo" data-slide-to="2"></li>
      </ol>-->

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <a href="#" class="item active" style="background-image: url(build/images/banner-interna.jpg);" alt="img" title="img">
        </a>      
      </div>

      <!-- Controls 
      <a class="left carousel-control" href="#banner-topo" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#banner-topo" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      -->
    </div>
</section>

<section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
        
    <div id="banner-mob" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <a href="#" class="item active">
            <img src="build/images/img-mob.jpg" alt="img" title="img">
        </a>       
      </div>

      <!-- Controls 
      <a class="left carousel-control" href="#banner-mob" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#banner-mob" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      -->
    </div>
</section>

<div class="container  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
  
  <div class="row">
    <div class="col-xs-12">
      
      <div class="interna">
        
        <h1 class="titulo">VAI BEM PARA O CLIENTE</h1>
        <p>O Cartão Vai Bem é uma opção prática e segura de garantir consultas, exames e procedimentos de qualidade     ligados a saúde e a preços acessíveis para você e sua família.
          <Br><Br>
                    O Cartão Vai Bem não é um plano de saúde nem um seguro. É um Cartão Pré-pago, que assim como um celular pré-pago, você precisa carregar para utilizar.
          <Br><Br>
                    Simples, acessível e prático, o Cartão Vai Bem é um meio de pagamento inovador que aproxima os clientes aos profissionais da saúde.
          <Br><Br>
          <strong>Além disso, você pode contar com:</strong>

        </p>

        <div class="row">
          
          <div class="col-md-4">
            
            <p>Descontos em 
            medicamentos em toda 
            rede Epharma;
            </p>

          </div>

          <div class="col-md-4">
            
            <p>Até 5 adicionais para 
              sua famíliaa e a 
              mensalidade é única 
              para todos;
            </p>

          </div>

          <div class="col-md-4">
            
            <p>Sem limite de uso ou 
              idade;
            </p>

          </div>

        </div>

        <div class="row">

          <div class="col-md-4">
            
            <p>Sem carência;</p>

          </div>

          <div class="col-md-4">
            
            <p>Agendamento de 
              consultas através da 
              equipe Vai Bem.
            </p>

          </div>

        </div>

        <h2 style="text-transform: uppercase; margin-top: 100px; display: block;">Confira o quanto você já pode começar a economizar</h2>

        <table class="table table-bordered tb-servicos">
          
          <tr class="th_cor">            
            <th>Serviço</th>
            <th>Valor médio de mercado</th>
            <th>Valor com Bem Mais</th>
            <th>Economia</th>
          </tr>
          <tr>
            <td>Consulta com Dentista</td>
            <td>R$ 220,00</td>
            <td>R$ 85,00 </td>
            <td>R$ 85,00 </td>
          </tr>
          <tr>
            <td>Consulta com Dentista</td>
            <td>R$ 220,00</td>
            <td>R$ 85,00 </td>
            <td>R$ 85,00 </td>
          </tr>
          <tr>
            <td>Consulta com Dentista</td>
            <td>R$ 220,00</td>
            <td>R$ 85,00 </td>
            <td>R$ 85,00 </td>
          </tr>
          <tr>
            <td>Consulta com Dentista</td>
            <td>R$ 220,00</td>
            <td>R$ 85,00 </td>
            <td>R$ 85,00 </td>
          </tr>
          <tr>
            <td>Consulta com Dentista</td>
            <td>R$ 220,00</td>
            <td>R$ 85,00 </td>
            <td>R$ 85,00 </td>
          </tr>
          <tr>
            <td>Consulta com Dentista</td>
            <td>R$ 220,00</td>
            <td>R$ 85,00 </td>
            <td>R$ 85,00 </td>
          </tr>

        </table>

        <a href="#" class="bt_padrao adq">Adquira o seu Cartão Vai Bem <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

        <div class="compartilhe">
            
            <p>Compartilhe: </p>

            <div class="box_midia"><img src="build/images/midia.jpg"></div>

        </div>

      </div>

    </div>
  </div>

</div>


<?php include('footer.php'); ?>