var toggle = document.querySelector('.toggle');
var header = document.querySelector('header');

toggle.onclick = function() {
    if(header.className.indexOf('ativo') == -1) {
        header.className += ' ativo';
        toggle.className += ' ativo';
    }
    else {
        header.className =
            header.className.slice(0, header.className.indexOf(' ativo')) + header.className.slice(header.className.indexOf(' ativo') + 6);

        toggle.className =
            toggle.className.slice(0, toggle.className.indexOf(' ativo')) + toggle.className.slice(toggle.className.indexOf(' ativo') + 6)
    }
}

// by Remerson