<footer class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.3s">

    <div class="container">
        
        <div class="row">
            
            <div class="col-md-3">
                <div class="marca">
                    <a href="index.php"><img src="build/images/logo.png" alt="Vai bem" title="Vai bem"></a>
                </div>
            </div>

            <div class="col-md-7">
                <nav>
                    
                    <ul>
                        <h3>Institucional</h3>
                        <li><a href="#">Sobre o Vai bem</a></li>
                        <li><a href="#">Nóticias</a></li>
                    </ul>

                    <ul>
                        <h3>Legal e Privacidade</h3>
                        <li><a href="#">Termos de uso</a></li>
                        <li><a href="#">Política de privacidade</a></li>
                    </ul>

                    <ul>
                        <h3>Recursos</h3>
                        <li><a href="#">Perguntas Frequentes</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-md-2 midas">
                
                <h3>Nas mídias</h3>

                <a href="#" title="Facebook" class="face"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#" title="Youtube" class="youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

            </div>
                
            <div class="col-xs-12">
                <p>© Copyright 1996 - 2016 Cartão VaiBem. All rights reserved.</p>
            </div>

        </div>

    </div>

</footer>



<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="http://malsup.github.io/jquery.cycle2.js"></script>
<script src="http://malsup.github.io/jquery.cycle2.carousel.js"></script>
<script src="build/js/scripts.min.js" type="text/javascript"></script>

<script>





        $(window).load(function() {
            var wow = new WOW({
                boxClass: 'wow',
                animateClass: 'animated',
                offset: 100,
                mobile: false,
                live: true
            });
            wow.init();
        });


    
</script>


</body>

</html>