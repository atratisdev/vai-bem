<?php include('header.php'); ?>




<section class="banner_principal wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
        
    <div id="banner-topo" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#banner-topo" data-slide-to="0" class="active"></li>
        <li data-target="#banner-topo" data-slide-to="1"></li>
        <li data-target="#banner-topo" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <a href="#" class="item active" style="background-image: url(build/images/banner.jpg);" alt="img" title="img">
        </a>
        <a href="#" class="item" style="background-image: url(build/images/banner2.jpg);" alt="img" title="img">
        </a>
        <a href="#" class="item" style="background-image: url(build/images/banner3.jpg);" alt="img" title="img">
        </a>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#banner-topo" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#banner-topo" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</section>

<section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
        
    <div id="banner-mob" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <a href="#" class="item active">
            <img src="build/images/banner-mob.jpg" alt="img" title="img">
        </a>
        <a href="#" class="item">
            <img src="build/images/banner-mob2.jpg" alt="img" title="img">
        </a>
        <a href="#" class="item">
            <img src="build/images/banner-mob3.jpg" alt="img" title="img">
        </a>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#banner-mob" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#banner-mob" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</section>

<p class="barra fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
    Conheça as vantagens do <strong>Cartão Vai Bem</strong>
    <span class="x"><i class="fa fa-chevron-down u-animation--pulse" aria-hidden="true"></i></span>
</p>

<section class="porque  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">        
        <h2>
          <strong>VAI BEM</strong>
          porque tem:
        </h2>
      </div>
    </div>

    <div class="row">   

      <div class="col-md-4 col-xs-12">        
        <div class="item">
          <div class="box_img fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
            <img src="build/images/i01.svg" alt="img">
          </div>
          <div class="fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
            <h3>Consultas e procedimentos
              a preços reduzidos</h3>
              <p>Com o Cartão Vai Bem você tem acesso a uma ampla rede de serviços ligadas a saúde e bem-estar como: médicos, clínicas, laboratórios, dentistas, nutricionistas, psicólogos, academias, assessorias esportivas e muito mais.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">        
          <div class="item">
            <div class="box_img   fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
              <img src="build/images/i02.png" alt="img">
            </div>
            <div class=" fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
            <h3>Descontos em medicamentos</h3>
            <p>O Cartão Vai bem, em parceria com a rede Epharma, oferece milhares de medicamentos com descontos exclusivos. Clique aqui e veja onde comprar medicamentos a preços reduzidos com seu Cartão Vai Bem.</p>
          </div>
          </div>
        </div>
        <div class="col-md-4 col-xs-12">        
          <div class="item">
            <div class="box_img   fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
              <img src="build/images/i03.png" alt="img">
            </div>
            <div class=" fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
            <h3>O cartão da sua família</h3>
            <p>Com apenas a mensalidade de um cartão você tem cobertura para toda a família*. Solicite já o adicional de quem você ama.</p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">   
        <div class="col-xs-12 col-md-2"></div>

        <div class="col-md-4 col-xs-12">        
          <div class="item">
            <div class="box_img   fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
              <img src="build/images/i04.png" alt="img">
            </div>
            <div class=" fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
            <h3>
              Sem carência, limite de uso ou idade
            </h3>
            <p>Solicite seu cartão, carregue e agende sua consulta ou procedimento na hora. Utilize seu cartão sempre que quiser.</p>
            </div>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">        
          <div class="item">
            <div class="box_img   fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
              <img src="build/images/i05.svg" alt="img">
            </div>
            <div class=" fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
            <h3>Agendamento de consultas</h3>
            <p>Entre em contato com a equipe Vai Bem e agende sua consulta ou procedimento, com tranquilidade e comodidade</p>
          </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-2"></div>      
      </div>

      <div class="row">
        <div class="col-xs-12">

          <a href="#" class="bt_padrao">Adquira o seu Cartão Vai Bem</a>

          <span class="cf">*Cônjuge e filhos</span>

        </div>
      </div>

    </div>

  </section>


<section class="funciona  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">  
  <div class="container">
    <div class="row">
      <div class="col-xs-12">        
      <div class="seta_funciona"><i class="fa fa-chevron-down u-animation--pulse" aria-hidden="true"></i></div>
        <h2>
          <strong>VAI BEM</strong>
          como funciona?
        </h2>
      </div>
    </div>

    <div class="row">   

      <div class="col-xs-12 col-md-1"></div>

      <div class="col-md-4 col-xs-12">        
          <a href="#" class="item">
            <div class="box_img img_solicite">
              <img src="build/images/cartao.png" alt="img">
            </div>
            <p><strong>Solicite seu cartão</strong> e tenha acesso a uma ampla rede de médicos, clínicas, laboratórios e serviços ligados a saúde e bem-estar. 
            </p>
          </a>
      </div>


      <div class="col-xs-12 col-md-2"></div>

      <div class="col-md-4 col-xs-12">        
        <a href="#" class="item">
          <div class="box_img img_agende">
            <img src="build/images/agenda.png" alt="img">
          </div>
          <p>Agende sua consulta/exame ou procedimento através do nosso <strong>Chat</strong> ou através da nossa Central de Atendimento.</p>
        </a>
      </div>


      <div class="col-xs-12 col-md-1"></div>
    </div>

      <div class="row">   
        <div class="col-xs-12 col-md-1"></div>

        <div class="col-md-4 col-xs-12">        
          <a href="#" class="item">
            <div class="box_img img_carregue">
              <img src="build/images/cartao2.png" alt="img">
            </div>
            <p>Carregue seu cartão com o valor da consulta/exame ou procedimento.</p>
          </a>
        </div>

        <div class="col-xs-12 col-md-2"></div>

        <div class="col-md-4 col-xs-12">        
          <a href="#" class="item">
            <div class="box_img img_consulta">
              <img src="build/images/maquina.png" alt="img">
            </div>
            <p>Vá até o local da sua consulta/exame ou procedimento no dia e hora agendados, com sua senha decorada e cartão carregado para realizar o pagamento.</p>
          </a>
        </div>

        <div class="col-xs-12 col-md-1"></div>      
      </div>

      <div class="row">
        <div class="col-xs-12">
          
          <a href="#" class="bt_padrao">Adquira o seu Cartão Vai Bem  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

        </div>
      </div>

    </div>

  <span class="bg_fun_02"></span>
</section>



<section class="videos  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">  
  <div class="container">
    <div class="row">

      <div class="col-xs-12">

        <div class="cycle-slideshow" 
        data-cycle-swipe=true
        data-cycle-timeout=0
        data-cycle-swipe-fx=scrollHorz
        data-cycle-slides="li"
        >

        <div class="cycle-pager"></div>

        <ul class="cycle-slideshow row" data-cycle-slides="li">
            <li>
              <div class="col-md-7   fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s"><span class="monitor"><iframe width="533" height="300" src="https://www.youtube.com/embed/aVRFVg0wS_Y" frameborder="0" allowfullscreen></iframe></span></div>
              <div class="col-md-5   fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2>VAI BEM</h2>
                <h3>Como solicitar o meu?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim velit ornare magna pulvinar, quis feugiat turpis viverra. Vestibulum vel tincidunt magna, nec luctus nibh. Pellentesque quis imperdiet mauris.</p>
              </div>
            </li>
            <li>
              <div class="col-md-7"><span class="monitor"><iframe width="533" height="300" src="https://www.youtube.com/embed/aVRFVg0wS_Y" frameborder="0" allowfullscreen></iframe></span></div>
              <div class="col-md-5">
                <h2>VAI BEM 2</h2>
                <h3>Como solicitar o meu?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim velit ornare magna pulvinar, quis feugiat turpis viverra. Vestibulum vel tincidunt magna, nec luctus nibh. Pellentesque quis imperdiet mauris.</p>
              </div>
            </li>
            <li>
              <div class="col-md-7"><span class="monitor"><iframe width="533" height="300" src="https://www.youtube.com/embed/aVRFVg0wS_Y" frameborder="0" allowfullscreen></iframe></span></div>
              <div class="col-md-5">
                <h2>VAI BEM 3</h2>
                <h3>Como solicitar o meu?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim velit ornare magna pulvinar, quis feugiat turpis viverra. Vestibulum vel tincidunt magna, nec luctus nibh. Pellentesque quis imperdiet mauris.</p>
              </div>
            </li>

        </ul>

        </div>
      </div>
    </div>
  </div>
</section>


<section class="newsletter  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
  
  <div class="container">
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <h2 class="barra">VAI BEM</h2>
        <h3 class="sub_titulo">ficar por dentro das novidades.</h3>
        <form action="" class="row">          
          <div class="col-md-5"><input type="text" class="inpt inpt_nome   fadeInLeft" data-wow-duration="1s" data-wow-delay="1s" placeholder="Digite aqui seu nome"></div>
          <div class="col-md-5"><input type="text" class="inpt inpt_email   fadeInRight" data-wow-duration="1s" data-wow-delay="1s" placeholder="Digite aqui seu e-mail"></div>
          <div class="col-md-2"><input type="submit" class="bt_padrao  fadeInRight" data-wow-duration="1s" data-wow-delay="2s" value="Assinar"></div>
        </form>
      </div>
      <div class="col-md-1"></div>
    </div>
  </div>

</section>


<?php include('footer.php'); ?>