<?php include('header.php'); ?>

<section class="banner_blog wow fadeInDown  hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">        
  <div class="item-blog" style="background-image: url(build/images/blog-banner.jpg); height: 279px; background-position: center;" alt="img" title="img">
  </div>
</section>

<section class="banner-mob wow fadeInDown hidden-lg hidden-md" data-wow-duration="1s" data-wow-delay="0.5s">
  <div class="item-blog">
      <img src="build/images/img-mob.jpg" alt="img" title="img">
  </div>
</section>

<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
  
  <div class="row">

    <div class="interna">
    
      <div class="breadcrumbs col-xs-12">      
        <ul>        
          <li><a href="index.php">Página inicial <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a></li>
          <li><a href="index.php">Página inicial teste 2 <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a></li>
          
          <li class="active">Notícias</li>
        </ul>
      </div>

      <div class="col-xs-12">
        
        <h1 class="titulo">
          Notícias
        </h1>

        <section class="carrosel_blog  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">  
          
                <div class="cycle-slideshow" 
                data-cycle-swipe=true
                data-cycle-timeout=0
                data-cycle-swipe-fx=scrollHorz
                data-cycle-slides="li"
                >


                  <ul class="cycle-slideshow" data-cycle-slides="li">
                      <li>
                        <div class="box_foto"><span class="foto"><img src="build/images/banner-blog-noticias.jpg"></span></div>
                        <div class="box_texto">
                          <h2>Acesse seu extrato de onde estiver</h2>
                          <p>Lorem ipsum dolor med lorem ipsum dolor med ipsum dolor med lorem ipsum dolor med</p>
                          <a href="#">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                      </li>
                      <li>
                        <div class="box_foto"><span class="foto"><img src="build/images/banner-blog-noticias.jpg"></span></div>
                        <div class="box_texto">
                          <h2>Acesse seu extrato de onde estiver 2222</h2>
                          <p>Lorem ipsum dolor med lorem ipsum dolor med ipsum dolor med lorem ipsum dolor med</p>
                          <a href="#">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                      </li>
                      <li>
                        <div class="box_foto"><span class="foto"><img src="build/images/banner-blog-noticias.jpg"></span></div>
                        <div class="box_texto">
                          <h2>Acesse seu extrato de onde estiver 333</h2>
                          <p>Lorem ipsum dolor med lorem med lorem ipsum dolor med ipsum dolor med ipsum dolor med lorem ipsum dolor med</p>
                          <a href="#">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                      </li>

                      <br clear="all" />

                  </ul>


                  <div class="cycle-pager"></div>

                </div>
          
        </section>
      </div>

      <div class="col-md-9">        
        <div class="post">
          <div class="item">          
            <div class="imagem">
              <a href="post.php"><img src="build/images/ft.jpg" alt="img" title="img"></a>
            </div>          
            <div class="box_texto">            
              <div class="data">30/05/2016</div>
              <div class="tags"><a href="#">Dicas</a><a href="#">Clipping</a></div>
              <h2>
                <a href="post.php">Lorem ipsum dolor sit amet, consectetur 
                adipiscing elit. Sed vulputate venenatis 
                magna, eu sagittis odio molestie sit oles
                tie sit amet. </a>
              </h2>
              <p>Lorem ipsum dolor sit amet, consectetur ttadipiscing elit. Mauris laoreet metus dui, non consectetur quam tristique et.
              </p>
              <a href="post.php" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>

        <div class="post">
          <div class="item">          
            <div class="imagem">
              <a href="post.php"><img src="build/images/ft.jpg" alt="img" title="img"></a>
            </div>          
            <div class="box_texto">            
              <div class="data">30/05/2016</div>
              <div class="tags"><a href="#">Dicas</a><a href="#">Clipping</a></div>
              <h2>
                <a href="post.php">Lorem ipsum dolor sit amet, consectetur 
                adipiscing elit. Sed vulputate venenatis 
                magna, eu sagittis odio molestie sit oles
                tie sit amet. </a>
              </h2>
              <p>Lorem ipsum dolor sit amet, consectetur ttadipiscing elit. Mauris laoreet metus dui, non consectetur quam tristique et.
              </p>
              <a href="post.php" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>

        <div class="post">
          <div class="item">          
            <div class="imagem">
              <a href="post.php"><img src="build/images/ft.jpg" alt="img" title="img"></a>
            </div>          
            <div class="box_texto">            
              <div class="data">30/05/2016</div>
              <div class="tags"><a href="#">Dicas</a><a href="#">Clipping</a></div>
              <h2>
                <a href="post.php">Lorem ipsum dolor sit amet, consectetur 
                adipiscing elit. Sed vulputate venenatis 
                magna, eu sagittis odio molestie sit oles
                tie sit amet. </a>
              </h2>
              <p>Lorem ipsum dolor sit amet, consectetur ttadipiscing elit. Mauris laoreet metus dui, non consectetur quam tristique et.
              </p>
              <a href="post.php" class="lendo">Continue lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>

        <br clear="all" />

        <nav class="pag">
          <ul>
            <li><a class="prevpostslink" rel="next" href="#">&lsaquo; Anterior </a></li>
            <li><a class="current">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">20</a></li>
            <li><a class="nextpostslink" rel="next" href="#">Próxima &rsaquo;</a></li>
          </ul>
        </nav>


      </div>
      <aside class="col-md-3">        
        <div class="categoria">
          
          <h2>CATEGORIAS</h2>
          <ul>

            <li><a href="#">Todas</a></li>
            <li><a href="#" class="current">Casos de Sucesso</a></li>
            <li><a href="#">Depoimento</a></li>
            <li><a href="#">Iluminação</a></li>
            <li><a href="#">Eventos</a></li>

          </ul>
        </div>
        
        <section class="newsletter  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
          
          <h2>VAI BEM</h2>
          <h3>ficar por dentro das novidades.</h3>
          <form>          
            <div><input type="text" class="inpt inpt_nome" placeholder="Digite aqui seu nome"></div>        
            <div><input type="text" class="inpt inpt_email" placeholder="Digite aqui seu e-mail"></div>     
            <div><input type="submit" class="bt_padrao" value="Assinar"></div>  
          </form>
                      
        </section>

      </aside>


    </div>
  </div>
</div>





<?php include('footer.php'); ?>